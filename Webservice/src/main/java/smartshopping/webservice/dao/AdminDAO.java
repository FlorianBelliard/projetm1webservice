/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.dao;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.jasypt.util.password.BasicPasswordEncryptor;
import smartshopping.webservice.entity.Admin;
import smartshopping.webservice.exceptions.DbException;
import smartshopping.webservice.util.HibernateUtil;

/**
 * DAO pour les administrateurs
 *
 * @author FLORIAN
 */
public class AdminDAO extends GenericDAO<Admin> {

    /**
     * Indique si l'utilisateur existe, et si c'est le cas le retourne
     *
     * @param login Login de l'utilisateur
     * @param password Mot de passe de l'utilisateur
     * @return Utilisateur
     */
    public Admin isAdminExist(String login, String password) {
        Admin admin = null;
        Admin adminFound = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Admin.class)
                    .add(Restrictions.eq("login", login));
            adminFound = (Admin) criteria.uniqueResult();
        } catch (HibernateException e) {
            throw new DbException(e.getLocalizedMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }

        if (adminFound != null) {
            BasicPasswordEncryptor encryptor = new BasicPasswordEncryptor();

            try {
                if (encryptor.checkPassword(password, adminFound.getPassword())) {
                    admin = adminFound;
                }
            } catch (EncryptionOperationNotPossibleException ex) {
                admin = null;
            }
        }

        return admin;
    }
}
