/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.dao;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import smartshopping.webservice.entity.Brand;
import smartshopping.webservice.exceptions.DbException;
import smartshopping.webservice.util.HibernateUtil;

/**
 * DAO pour les marques
 * @author FLORIAN
 */
public class BrandDAO extends GenericDAO<Brand> {
    
    /**
     * Récupère les marques présentes dans la chaîne
     * @param id Identifiant de la chaîne
     * @return Liste des marques
     */
    public List<Brand> getBrandsForChain(long id) {
        List<Brand> brands = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Brand.class)
                    .setFetchMode("chains", FetchMode.JOIN)
                    .createAlias("chains", "c")
                    .add(Restrictions.eq("c.id", id));
            brands = criteria.list();
        } catch (HibernateException e) {
            throw new DbException(e.getLocalizedMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return brands;
    }
}
