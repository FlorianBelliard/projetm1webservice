/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.dao;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import smartshopping.webservice.entity.Chain;
import smartshopping.webservice.exceptions.DbException;
import smartshopping.webservice.util.HibernateUtil;

/**
 * DAO pour les chaînes
 * @author FLORIAN
 */
public class ChainDAO extends GenericDAO<Chain> {

    /**
     * Récupère les chaînes dans lesquelles la marque est présente
     * @param id Identifiant de la marque
     * @return Liste des chaînes
     */
    public List<Chain> getChainsForBrand(long id) {
        List<Chain> chains = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Chain.class)
                    .setFetchMode("brands", FetchMode.JOIN)
                    .createAlias("brands", "b")
                    .add(Restrictions.eq("b.id", id));
            chains = criteria.list();
            session.close();
        } catch (HibernateException e) {
            throw new DbException(e.getLocalizedMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return chains;
    }
}
