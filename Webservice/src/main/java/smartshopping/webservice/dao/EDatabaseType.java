/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.dao;

/**
 * Enumération contenant les différents types présents en BDD
 * @author FLORIAN
 */
public enum EDatabaseType {

    STRING,
    DATE,
    LONG,
    INT,
    DECIMAL
}
