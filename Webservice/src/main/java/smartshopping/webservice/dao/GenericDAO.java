/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import smartshopping.webservice.dao.interfaces.ISearchable;
import smartshopping.webservice.entity.Entity;
import smartshopping.webservice.exceptions.DbException;
import smartshopping.webservice.util.HibernateUtil;

/**
 * DAO génrique qui permet d'effectuer les actions de base sur la base de
 * données
 *
 * @author FLORIAN
 * @param <T>
 */
public abstract class GenericDAO<T extends Entity> {

    /**
     * Sauvegarde une entité en base de données
     *
     * @param entity Entité
     */
    public void persist(T entity) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.persist(entity);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            throw new DbException(e.getLocalizedMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * Met à jour une entité en base de données
     *
     * @param entity Entité
     */
    public void merge(T entity) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.merge(entity);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            throw new DbException(e.getLocalizedMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * Supprime une entité en base de données
     *
     * @param entity Entité
     */
    public void delete(T entity) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(entity);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            throw new DbException(e.getLocalizedMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * Récupère une entité à partir de son identifiant
     *
     * @param type Type de l'entité
     * @param id Identifiant de l'entité
     * @return Entité
     */
    public T getById(Class<T> type, Long id) {
        T entity = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            entity = (T) session.get(type, id);
        } catch (HibernateException e) {
            throw new DbException(e.getLocalizedMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return entity;
    }

    /**
     * Récupère une entité à partir de son identifiant
     *
     * @param type Type de l'entité
     * @param id Identifiant de l'entité
     * @param fetchs Liste des propriétés à charger
     * @return Entité
     */
    public T getById(Class<T> type, Long id, Set<String> fetchs) {
        T entity = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(type);

            for (String fetch : fetchs) {
                criteria = criteria.setFetchMode(fetch, FetchMode.JOIN);
            }
            criteria.add(Restrictions.eq("id", id));

            entity = (T) criteria.uniqueResult();
        } catch (HibernateException e) {
            throw new DbException(e.getLocalizedMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return entity;
    }

    /**
     * Récupère toutes les entités d'un type
     *
     * @param type Type des entités
     * @return Liste des entités
     */
    public List<T> getAll(Class<T> type) {
        List<T> entities = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(type);
            entities = criteria.list();
        } catch (HibernateException e) {
            throw new DbException(e.getLocalizedMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return entities;
    }

    /**
     * Recherche générique pour une entité implémentant ISearchable
     *
     * @param type Type de l'entité
     * @param value Valeur recherchée
     * @param filters Liste des filtres à appliquer pour la recherche
     * @return Liste des résultats
     */
    public List<T> search(Class<T> type, String value, List<String> filters) {

        //Si le DAO courant n'implémente pas ISearchable
        if (!ISearchable.class.isAssignableFrom(this.getClass())) {
            throw new DbException("La recherche n'est pas autorisée pour cette entité.");
        }

        List<T> entities = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(type);

            Disjunction or = Restrictions.disjunction();
            Set<String> aliases = new HashSet<String>();

            //criteria = loadNeededProperties(criteria, aliases);
            //Parcours la liste des champs sur lesquels on peut effectuer une recherche
            for (String field : ((ISearchable) this).getSearchableFields().keySet()) {
                String neededAlias = createNeededAliases(field, criteria, aliases);
                or.add(Restrictions.like(neededAlias, (String) convertStringToCorrectDatabaseObject(value, ((ISearchable) this).getSearchableFields().get(field)), MatchMode.ANYWHERE));
            }

            criteria.add(or);

            //Parcours la liste des champs sur lesquels on peut filtrer la recherche
            List<String> filterableFields = new ArrayList<String>(((ISearchable) this).getFilterableFields().keySet());
            for (int i = 0; i < filters.size(); i++) {
                String field = (String) filterableFields.get(i);
                String neededAlias = createNeededAliases(field, criteria, aliases);
                criteria.add(Restrictions.eq(neededAlias, convertStringToCorrectDatabaseObject(filters.get(i), ((ISearchable) this).getFilterableFields().get(field))));
            }
            criteria.setMaxResults(50);
            entities = criteria.list();
        } catch (HibernateException e) {
            throw new DbException(e.getLocalizedMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return entities;
    }

    /**
     * Crée les différents alias nécessaires à la requête et les ajoute au
     * criteria
     *
     * @param field Champ à atteindre
     * @param criteria Criteria de la requête
     * @param aliases Liste des alias déjà utilisés
     * @return Alias pour atteindre le champ field
     */
    private String createNeededAliases(String field, Criteria criteria, Set<String> aliases) {
        String neededAlias = field;
        String[] split = field.split("\\.");

        if (split.length > 1) {
            int i;
            for (i = 0; i < split.length - 1; i++) {
                String alias = split[i].substring(0, 3);

                if (!aliases.contains(alias)) {

                    if (i == 0) {
                        criteria = criteria.createAlias(split[i], alias);
                    } else {
                        criteria = criteria.createAlias(split[i - 1] + "." + split[i], alias);
                    }
                    aliases.add(alias);
                }

                split[i] = alias;
            }

            neededAlias = split[i - 1] + "." + split[i];
        }

        return neededAlias;
    }

    /**
     * Convertit une valeur sous forme de chaîne dans le format correspondant au
     * type en base de données
     *
     * @param value Valeur
     * @param databaseType Type à donner à la nouvelle valeur
     * @return Nouvelle valeur
     */
    protected Object convertStringToCorrectDatabaseObject(String value, EDatabaseType databaseType) {
        Object newValue = value;
        switch (databaseType) {
            case LONG:
                newValue = Long.parseLong(value);
                break;
            case DECIMAL:
                newValue = Float.parseFloat(value);
                break;
            case INT:
                newValue = Integer.parseInt(value);
                break;
            case DATE:
                newValue = Date.valueOf(value);
            default:
                break;
        }

        return newValue;
    }
}
