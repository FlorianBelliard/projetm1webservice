/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.dao;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import smartshopping.webservice.dao.interfaces.ISearchable;
import smartshopping.webservice.entity.Item;
import smartshopping.webservice.entity.Reference;
import smartshopping.webservice.entity.Store;
import smartshopping.webservice.exceptions.DbException;
import smartshopping.webservice.util.HibernateUtil;

/**
 * DAO pour les articles
 *
 * @author FLORIAN
 */
public class ItemDAO extends GenericDAO<Item> implements ISearchable {

    /**
     * Retourne la liste des champs autorisés pour la recherche
     *
     * @return Map des champs
     */
    @Override
    public Map<String, EDatabaseType> getSearchableFields() {
        Map<String, EDatabaseType> fields = new HashMap<String, EDatabaseType>();
        fields.put("reference.code", EDatabaseType.STRING);
        fields.put("reference.label", EDatabaseType.STRING);
        fields.put("reference.brand.label", EDatabaseType.STRING);

        return fields;
    }

    /**
     * Retourne la liste des champs autorisés pour filtrer la recherche
     *
     * @return Map des champs
     */
    @Override
    public LinkedHashMap<String, EDatabaseType> getFilterableFields() {
        LinkedHashMap<String, EDatabaseType> fields = new LinkedHashMap<String, EDatabaseType>();
        fields.put("store.id", EDatabaseType.LONG);
        return fields;
    }

    /**
     * Récupère les articles de la référence
     *
     * @param id Identifiant de la référence
     * @return Liste des articles
     */
    public List<Item> getItemsForReference(long id) {
        List<Item> items = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Item.class)
                    .setFetchMode("reference", FetchMode.JOIN)
                    .createAlias("reference", "r")
                    .add(Restrictions.eq("r.id", id));
            items = criteria.list();
        } catch (HibernateException e) {
            throw new DbException(e.getLocalizedMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return items;
    }

    /**
     * Récupère les articles d'un magasin
     *
     * @param id Identifiant du magasin
     * @return Liste des articles
     */
    public List<Item> getItemsForStore(long id) {
        List<Item> items = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Item.class)
                    .setFetchMode("store", FetchMode.JOIN)
                    .createAlias("store", "s")
                    .add(Restrictions.eq("s.id", id));
            items = criteria.list();
        } catch (HibernateException e) {
            throw new DbException(e.getLocalizedMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return items;
    }

    /**
     * Récupère un article à partir d'une référence et d'un magasin
     *
     * @param reference Référence
     * @param store Magasin
     * @return Article
     */
    public Item getItemOfReferenceFromStore(Reference reference, Store store) {
        Item item = null;
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Item.class)
                    .createAlias("store", "s")
                    .createAlias("reference", "r")
                    .add(Restrictions.eq("s.id", store.getId()))
                    .add(Restrictions.eq("r.id", reference.getId()));
            item = (Item) criteria.uniqueResult();
        } catch (HibernateException e) {
            throw new DbException(e.getLocalizedMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return item;
    }
}
