/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.dao;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import smartshopping.webservice.entity.List;
import smartshopping.webservice.exceptions.DbException;
import smartshopping.webservice.util.HibernateUtil;

/**
 * DAO pour les listes
 * @author FLORIAN
 */
public class ListDAO extends GenericDAO<List> {
    
    /**
     * Permet de s'assurer qu'une liste appartient bien à un utilisateur
     * @param listid Identifiant de la liste 
     * @param userid Identifiant de l'utilisateur
     * @return Liste trouvée
     */
    public List getById(long listid, long userid){
        List list = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(List.class)
                    .createAlias("user", "u")
                    .add(Restrictions.eq("id", listid))
                    .add(Restrictions.eq("u.id", userid));
            list = (List) criteria.uniqueResult();
        } catch (HibernateException e) {
            throw new DbException(e.getLocalizedMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return list;
    }
    
    /**
     * Récupère les listes d'un utilisateur
     * @param id Identifiant d'un utilisateur
     * @return Liste des listes
     */
    public java.util.List<List> getListsForUser(long id) {
        java.util.List<List> lists = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(List.class)
                    .setFetchMode("user", FetchMode.JOIN)
                    .createAlias("user", "u")
                    .add(Restrictions.eq("u.id", id));
            lists = criteria.list();
        } catch (HibernateException e) {
            throw new DbException(e.getLocalizedMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return lists;
    }
}
