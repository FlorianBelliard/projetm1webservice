/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.dao;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import smartshopping.webservice.dao.interfaces.IAssociationDAO;
import smartshopping.webservice.entity.Entity;
import smartshopping.webservice.entity.ListReference;
import smartshopping.webservice.exceptions.DbException;
import smartshopping.webservice.exceptions.NotFoundException;
import smartshopping.webservice.util.HibernateUtil;

/**
 * DAO pour les articles d'une liste
 *
 * @author FLORIAN
 */
public class ListReferenceDAO extends GenericDAO<ListReference> implements IAssociationDAO {

    /**
     * Permet de retourner un article dans une liste
     *
     * @param id1 Identfiant de la liste
     * @param id2 Identifiant de la référence
     * @return Article dans la liste
     */
    @Override
    public Entity getByIds(long id1, long id2) {
        ListReference entity = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(ListReference.class)
                    .add(Restrictions.eq("pk.list.id", id1))
                    .add(Restrictions.eq("pk.reference.id", id2));
            entity = (ListReference) criteria.uniqueResult();
        } catch (HibernateException e) {
            throw new DbException(e.getLocalizedMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }

        if (entity == null) {
            throw new NotFoundException(ListReference.class, id1, id2);
        }

        return entity;
    }

    /**
     * Récupère la liste des articles d'une liste
     *
     * @param id Identifiant de la liste
     * @return Liste des articles dans la liste
     */
    public List<ListReference> getListReferencesForList(long id) {
        List<ListReference> listReferences = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(ListReference.class)
                    .add(Restrictions.eq("pk.list.id", id));
            listReferences = criteria.list();
        } catch (HibernateException e) {
            throw new DbException(e.getLocalizedMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return listReferences;
    }

}
