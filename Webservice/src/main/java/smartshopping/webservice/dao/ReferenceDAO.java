/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.dao;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import smartshopping.webservice.dao.interfaces.ISearchable;
import smartshopping.webservice.entity.Reference;
import smartshopping.webservice.exceptions.DbException;
import smartshopping.webservice.util.HibernateUtil;

/**
 * DAO pour les références
 *
 * @author FLORIAN
 */
public class ReferenceDAO extends GenericDAO<Reference> implements ISearchable {

    /**
     * Récupère les références ayant cette marque
     *
     * @param id Identfiant de la marque
     * @return Liste des références
     */
    public List<Reference> getReferencesForBrand(long id) {
        List<Reference> references = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Reference.class)
                    .setFetchMode("brand", FetchMode.JOIN)
                    .createAlias("brand", "b")
                    .add(Restrictions.eq("b.id", id));
            references = criteria.list();
        } catch (HibernateException e) {
            throw new DbException(e.getLocalizedMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return references;
    }

    /**
     * Retourne la liste des champs autorisés pour la recherche
     *
     * @return Map des champs
     */
    @Override
    public Map<String, EDatabaseType> getSearchableFields() {
        Map<String, EDatabaseType> fields = new HashMap<String, EDatabaseType>();
        fields.put("code", EDatabaseType.STRING);
        fields.put("label", EDatabaseType.STRING);
        fields.put("brand.label", EDatabaseType.STRING);

        return fields;
    }

    @Override
    public LinkedHashMap<String, EDatabaseType> getFilterableFields() {
        return new LinkedHashMap<String, EDatabaseType>();
    }
}
