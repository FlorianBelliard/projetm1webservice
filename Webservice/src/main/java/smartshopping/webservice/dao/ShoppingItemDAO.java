/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.dao;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import smartshopping.webservice.dao.interfaces.IAssociationDAO;
import smartshopping.webservice.entity.Entity;
import smartshopping.webservice.entity.ShoppingItem;
import smartshopping.webservice.exceptions.DbException;
import smartshopping.webservice.exceptions.NotFoundException;
import smartshopping.webservice.util.HibernateUtil;

/**
 * DAO pour les articles d'une liste de courses
 *
 * @author FLORIAN
 */
public class ShoppingItemDAO extends GenericDAO<ShoppingItem> implements IAssociationDAO {

    /**
     * Récupère les articles d'une liste de courses
     *
     * @param id Identifiant de la liste de courses
     * @return Liste des articles
     */
    public java.util.List<ShoppingItem> getShoppingItemsForShopping(long id) {
        java.util.List<ShoppingItem> shoppingItems = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(ShoppingItem.class)
                    .add(Restrictions.eq("pk.shopping.id", id));
            shoppingItems = criteria.list();
        } catch (HibernateException e) {
            throw new DbException(e.getLocalizedMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return shoppingItems;
    }

    /**
     * Permet de retourner un article dans une liste de courses
     *
     * @param id1 Identfiant de la liste de course
     * @param id2 Identifiant du produit
     * @return Article dans la liste de courses
     */
    @Override
    public Entity getByIds(long id1, long id2) {
        ShoppingItem entity = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(ShoppingItem.class)
                    .add(Restrictions.eq("pk.shopping.id", id1))
                    .add(Restrictions.eq("pk.item.id", id2));
            entity = (ShoppingItem) criteria.uniqueResult();
        } catch (HibernateException e) {
            throw new DbException(e.getLocalizedMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }

        if (entity == null) {
            throw new NotFoundException(ShoppingItem.class, id1, id2);
        }

        return entity;
    }

}
