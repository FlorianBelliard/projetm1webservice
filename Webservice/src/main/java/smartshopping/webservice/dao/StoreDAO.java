/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.dao;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import smartshopping.webservice.dao.interfaces.ISearchable;
import smartshopping.webservice.entity.Store;
import smartshopping.webservice.exceptions.DbException;
import smartshopping.webservice.util.HibernateUtil;

/**
 * DAO pour les magasins
 *
 * @author FLORIAN
 */
public class StoreDAO extends GenericDAO<Store> implements ISearchable {

    /**
     * Récupère les magasins d'un chaine
     *
     * @param id Identifiant de la chaîne
     * @return Liste des magasins
     */
    public List<Store> getStoresForChain(long id) {
        List<Store> stores = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Store.class)
                    .setFetchMode("chain", FetchMode.JOIN)
                    .createAlias("chain", "c")
                    .add(Restrictions.eq("c.id", id));
            stores = criteria.list();
            session.close();
        } catch (HibernateException e) {
            throw new DbException(e.getLocalizedMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return stores;
    }

    /**
     * Récupère la liste des magasins favoris d'un utilisateur
     *
     * @param id Identfiant de l'utilisateur
     * @return Liste des magasins favoris
     */
    public List<Store> getStoresForUser(long id) {
        List<Store> stores = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Store.class)
                    .setFetchMode("users", FetchMode.JOIN)
                    .createAlias("users", "u")
                    .add(Restrictions.eq("u.id", id));
            stores = criteria.list();
        } catch (HibernateException e) {
            throw new DbException(e.getLocalizedMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return stores;
    }

    /**
     * Retourne la liste des champs autorisés pour la recherche
     *
     * @return Map des champs
     */
    @Override
    public Map<String, EDatabaseType> getSearchableFields() {
        Map<String, EDatabaseType> fields = new HashMap<String, EDatabaseType>();
        fields.put("address", EDatabaseType.STRING);
        fields.put("position", EDatabaseType.STRING);
        fields.put("label", EDatabaseType.STRING);
        fields.put("chain.label", EDatabaseType.STRING);

        return fields;
    }

    @Override
    public LinkedHashMap<String, EDatabaseType> getFilterableFields() {
        return new LinkedHashMap<String, EDatabaseType>();
    }
}
