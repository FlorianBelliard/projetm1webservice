/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.dao;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.jasypt.util.password.BasicPasswordEncryptor;
import smartshopping.webservice.entity.User;
import smartshopping.webservice.exceptions.DbException;
import smartshopping.webservice.util.HibernateUtil;

/**
 * DAO pour les utilisateurs
 *
 * @author FLORIAN
 */
public class UserDAO extends GenericDAO<User> {

    /**
     * Indique si l'utilisateur existe, et si c'est le cas le retourne
     *
     * @param login Login de l'utilisateur
     * @param password Mot de passe de l'utilisateur
     * @return Utilisateur
     */
    public User isUserExist(String login, String password) {
        User user = null;
        User userFound = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(User.class)
                    .add(Restrictions.eq("login", login));
            userFound = (User) criteria.uniqueResult();
        } catch (HibernateException e) {
            throw new DbException(e.getLocalizedMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }

        if (password == null) {
            user = userFound;
        } else if (userFound != null) {
            BasicPasswordEncryptor encryptor = new BasicPasswordEncryptor();
            try {
                if (encryptor.checkPassword(password, userFound.getPassword())) {
                    user = userFound;
                }
            } catch (EncryptionOperationNotPossibleException ex) {
                user = null;
            }
        }

        return user;
    }

    /**
     * Récupère les utilisateurs ayant en favori le magasin
     *
     * @param id Identifiant du magasin
     * @return Liste des utilisateurs
     */
    public List<User> getUsersForStore(long id) {
        List<User> users = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(User.class)
                    .setFetchMode("favoritesStores", FetchMode.JOIN)
                    .createAlias("favoritesStores", "f")
                    .add(Restrictions.eq("f.id", id));
            users = criteria.list();
        } catch (HibernateException e) {
            throw new DbException(e.getLocalizedMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return users;
    }

    /**
     * Ajoute en BDD un utilisateur
     *
     * @param user Utilisateur
     */
    @Override
    public void persist(User user) {
        User userFound = isUserExist(user.getLogin(), null);

        if (userFound == null) {
            //Encryption du message
            BasicPasswordEncryptor encryptor = new BasicPasswordEncryptor();
            user.setPassword(encryptor.encryptPassword(user.getPassword()));
            super.persist(user);
        } else {
            throw new DbException("Le login " + user.getLogin() + " est déjà utilisé.");
        }
    }
}
