/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package smartshopping.webservice.dao.interfaces;

import smartshopping.webservice.entity.Entity;

/**
 * Interface pour les DAO des entités représentant des associations
 * @author fbelliar
 */
public interface IAssociationDAO {
    
    /**
     * Permet de récupérer une entité à partir des identifiants composants sa clé primaire
     * @param id1 Identifiant 1
     * @param id2 Identifiant 2
     * @return 
     */
    Entity getByIds(long id1, long id2);
}
