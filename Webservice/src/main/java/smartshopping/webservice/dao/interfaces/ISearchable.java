/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.dao.interfaces;

import java.util.LinkedHashMap;
import java.util.Map;
import smartshopping.webservice.dao.EDatabaseType;

/**
 * Interface pour la recherche dans les entités
 * @author FLORIAN
 */
public interface ISearchable {

    /**
     * Retourne les champs autorisés pour la recherche
     * @return 
     */
    Map<String, EDatabaseType> getSearchableFields();

    /**
     * Retourne les champs autorisés pour filtrer la recherche
     * @return 
     */
    LinkedHashMap<String, EDatabaseType> getFilterableFields();
}
