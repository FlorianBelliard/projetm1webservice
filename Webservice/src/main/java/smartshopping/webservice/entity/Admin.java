/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Classe réprésentant un utilisateur
 *
 * @author FLORIAN
 */
@Entity
@Table(name = "admin", catalog = "smartshopping")
public class Admin extends Person {

    /**
     * Constructeur par défaut
     */
    public Admin() {
    }
}
