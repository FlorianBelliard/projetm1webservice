/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Classe représentant une marque de produits
 *
 * @author FLORIAN
 */
@Entity
@Table(name = "brand", catalog = "smartshopping")
public class Brand extends smartshopping.webservice.entity.Entity implements Serializable {

    /**
     * Identifiant de la marque
     */
    private long id;

    /**
     * Libellé de la marque
     */
    private String label;

    /**
     * Liste des chaînes de magasins parmi lesquelles la marque est présente
     */
    private Set<Chain> chains = new HashSet<Chain>(0);

    /**
     * Liste des références de la marque
     */
    private Set<Reference> references = new HashSet<Reference>(0);

    /**
     * Constructeur par défaut
     */
    public Brand() {
    }

    /**
     * Constructeur avec paramètres
     *
     * @param id Identifiant
     * @param label Libellé
     * @param chains Liste des châines
     * @param references Liste des références
     */
    public Brand(long id, String label, Set<Chain> chains, Set<Reference> references) {
        this.id = id;
        this.label = label;
        this.chains = chains;
        this.references = references;
    }

    /**
     * Obtient l'identifiant de la marque
     *
     * @return Identifiant
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public long getId() {
        return id;
    }

    /**
     * Définit l'identifiant de la marque
     *
     * @param id Identifiant
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Obtient le libellé de la marque
     *
     * @return Libellé
     */
    @Column(name = "label", nullable = false, length = 50)
    public String getLabel() {
        return label;
    }

    /**
     * Définit le libellé de la marque
     *
     * @param label Libellé
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Obtient la liste des chaînes de magasins parmi lesquelles la marque est
     * présente
     *
     * @return Liste des chaînes
     */
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "brands")
    public Set<Chain> getChains() {
        return chains;
    }

    /**
     * Définit la liste des chaînes de magasins parmi lesquelles la marque est
     * présente
     *
     * @param chains Liste des chaînes
     */
    public void setChains(Set<Chain> chains) {
        this.chains = chains;
    }

    /**
     * Obtient la liste des références associées à la marque
     *
     * @return
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "brand")
    public Set<Reference> getReferences() {
        return references;
    }

    /**
     * Définit la liste des références associées à la marque
     *
     * @param references
     */
    public void setReferences(Set<Reference> references) {
        this.references = references;
    }

}
