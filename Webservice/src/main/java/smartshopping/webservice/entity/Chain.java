/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Classe répresentant une chaîne de magasins
 *
 * @author FLORIAN
 */
@Entity
@Table(name = "chain", catalog = "smartshopping")
public class Chain extends smartshopping.webservice.entity.Entity implements Serializable {

    /**
     * Identifiant de la chaîne
     */
    private long id;

    /**
     * Libellé de la chaîne
     */
    private String label;

    /**
     * Liste des marques liées à la chaîne
     */
    private Set<Brand> brands = new HashSet<Brand>(0);

    /**
     * Liste des magasins de la chaîne
     */
    private Set<Store> stores = new HashSet<Store>(0);

    /**
     * Constructeur par défaut
     */
    public Chain() {
    }

    /**
     * Constrcuteur avec paramètres
     *
     * @param id Identifiant
     * @param label Libellé
     * @param brands Liste des marques
     * @param stores Liste des magasins
     */
    public Chain(long id, String label, Set<Brand> brands, Set<Store> stores) {
        this.id = id;
        this.label = label;
        this.brands = brands;
        this.stores = stores;
    }

    /**
     * Obtient l'identifiant de la chaîne
     *
     * @return Identifiant
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public long getId() {
        return id;
    }

    /**
     * Définit l'identifiant de la chaîne
     *
     * @param id Identifiant
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Obtient le libellé de la chaîne
     *
     * @return Libellé
     */
    @Column(name = "label", nullable = false, length = 50)
    public String getLabel() {
        return label;
    }

    /**
     * Définit le libellé de la chaîne
     *
     * @param label Libellé
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Obtient la liste des marques liées à la chaîne
     *
     * @return Liste des marques
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "chain_brand", catalog = "smartshopping", joinColumns = {
        @JoinColumn(name = "chainid", nullable = false, updatable = false)},
            inverseJoinColumns = {
                @JoinColumn(name = "brandid",
                        nullable = false, updatable = false)})
    public Set<Brand> getBrands() {
        return brands;
    }

    /**
     * Définit la liste des marques liées à la chaîne
     *
     * @param brands Liste des marques
     */
    public void setBrands(Set<Brand> brands) {
        this.brands = brands;
    }

    /**
     * Obtient la liste des magasins de la chaîne
     *
     * @return Liste des magasins
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "chain")
    public Set<Store> getStores() {
        return stores;
    }

    /**
     * Définit la liste des magasins de la chaîne
     *
     * @param stores Liste des magasins
     */
    public void setStores(Set<Store> stores) {
        this.stores = stores;
    }

}
