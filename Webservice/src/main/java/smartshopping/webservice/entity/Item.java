/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author FLORIAN
 */
@Entity
@Table(name = "item", catalog = "smartshopping")
public class Item extends smartshopping.webservice.entity.Entity implements Serializable {

    /**
     * Identifiant de l'article
     */
    private long id;

    /**
     * Prix de l'article
     */
    private double price;

    /**
     * Magasin auquel appartient l'article
     */
    private Store store;

    /**
     * Référence de l'article
     */
    private Reference reference;

    /**
     * Liste de la liste des listes contenant cet article
     */
    private Set<ShoppingItem> shoppingItems = new HashSet<ShoppingItem>(0);

    /**
     * Constructeur par défaut
     */
    public Item() {
    }

    /**
     * Constructeur avec paramètres
     *
     * @param id Identifiant
     * @param price Prix
     * @param store Magasin
     * @param reference
     * @param shoppingItems
     */
    public Item(long id, double price, Store store, Reference reference, Set<ShoppingItem> shoppingItems) {
        this.id = id;
        this.price = price;
        this.store = store;
        this.reference = reference;
        this.shoppingItems = shoppingItems;
    }

    /**
     * Obtient l'identifiant de l'article
     *
     * @return Identifiant
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    public long getId() {
        return id;
    }

    /**
     * Définit l'identifiant de l'article
     *
     * @param id Identifiant
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Obtient le prix de l'article
     *
     * @return Prix
     */
    @Column(name = "price", nullable = false, precision = 2)
    public double getPrice() {
        return price;
    }

    /**
     * Définit le prix de l'article
     *
     * @param price Prix
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * Obtient le magasin auquel appartient l'article
     *
     * @return Magasin
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "storeid", nullable = false)
    public Store getStore() {
        return store;
    }

    /**
     * Définit le magasin auquel appartient l'article
     *
     * @param store Magasin
     */
    public void setStore(Store store) {
        this.store = store;
    }

    /**
     * Obtient la référence de l'article
     *
     * @return Reference
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "referenceid", nullable = false)
    public Reference getReference() {
        return reference;
    }

    /**
     * Définit la référence de l'article
     *
     * @param reference Référence
     */
    public void setReference(Reference reference) {
        this.reference = reference;
    }

    /**
     * Obtient la liste des listes contenant cet article
     *
     * @return
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.item")
    public Set<ShoppingItem> getShoppingItems() {
        return shoppingItems;
    }

    /**
     * Définit la liste des liste contenant cet article
     *
     * @param shoppingItems
     */
    public void setShoppingItems(Set<ShoppingItem> shoppingItems) {
        this.shoppingItems = shoppingItems;
    }
}
