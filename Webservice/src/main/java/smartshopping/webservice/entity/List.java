/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Classe réprésentant une liste de courses
 *
 * @author FLORIAN
 */
@Entity
@Table(name = "list", catalog = "smartshopping")
public class List extends smartshopping.webservice.entity.Entity implements Serializable {

    /**
     * Identifiant de la liste
     */
    private long id;

    /**
     * Libellé de la liste
     */
    private String label;

    /**
     * Utilisateur à qui appartient la liste
     */
    private User user;

    /**
     * Liste de courses à effectuer ou effectuée
     */
    private Shopping shopping;

    /**
     * Liste des références contenues dans la liste
     */
    private Set<ListReference> listReferences = new HashSet<ListReference>(0);

    /**
     * Constructeur par défaut
     */
    public List() {
    }

    /**
     * Constructeur avec paramètres
     *
     * @param id Identifiant
     * @param label Libellé
     * @param user Utilsiateur
     * @param shopping Courses
     * @param listReferences Références
     */
    public List(long id, String label, User user, Shopping shopping, Set<ListReference> listReferences) {
        this.id = id;
        this.label = label;
        this.user = user;
        this.shopping = shopping;
        this.listReferences = listReferences;
    }

    /**
     * Obtient l'identifiant de la liste
     *
     * @return Identifiant
     */
    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false, unique = true)
    public long getId() {
        return id;
    }

    /**
     * Définit l'identifiant de la liste
     *
     * @param id Identifiant
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Obtient le libellé de la liste
     *
     * @return Libellé
     */
    @Column(name = "label", nullable = false, length = 50)
    public String getLabel() {
        return label;
    }

    /**
     * Définit le libellé de la liste
     *
     * @param label Libellé
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Obtient l'utilisateur à qui appartient la liste
     *
     * @return Utilisateur
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userid", nullable = false)
    public User getUser() {
        return user;
    }

    /**
     * Définit l'utilisateur à qui appartient la liste
     *
     * @param user Utilisateur
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Obtient les courses à effectuer de la liste
     *
     * @return Courses à effectuer
     */
    @OneToOne(fetch = FetchType.EAGER, mappedBy = "list")
    public Shopping getShopping() {
        return shopping;
    }

    /**
     * Définit les courses à effectuer de la liste
     *
     * @param shopping Course à effectuer
     */
    public void setShopping(Shopping shopping) {
        this.shopping = shopping;
    }

    /**
     * Obtient la liste des références contenues dans la liste
     *
     * @return Références
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.list", cascade = CascadeType.ALL)
    public Set<ListReference> getListReferences() {
        return listReferences;
    }

    /**
     * Définit la liste des références contenues dans la liste
     *
     * @param listReferences Références
     */
    public void setListReferences(Set<ListReference> listReferences) {
        this.listReferences = listReferences;
    }
}
