/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.entity;

import java.io.Serializable;
import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Classe réprésentant le fait qu'une référence soit dans une liste
 *
 * @author FLORIAN
 */
@Entity
@Table(name = "liste_reference", catalog = "smartshopping")
@AssociationOverrides({
    @AssociationOverride(name = "pk.list",
            joinColumns = @JoinColumn(name = "listid")),
    @AssociationOverride(name = "pk.reference",
            joinColumns = @JoinColumn(name = "referenceid"))})
public class ListReference extends smartshopping.webservice.entity.Entity implements Serializable {

    /**
     * Sert de clé primaire à cet classe
     */
    private ListReferenceId pk = new ListReferenceId();

    /**
     * Quantité de la référence dans la liste
     */
    private int quantity;

    /**
     * Constructeur par défaut
     */
    public ListReference() {
    }

    /**
     * Obtient l'objet qui sert de clé primaire
     *
     * @return Clé primaire
     */
    @EmbeddedId
    public ListReferenceId getPk() {
        return pk;
    }

    /**
     * Définit l'objet qui sert de clé primaire
     *
     * @param pk Clé primaire
     */
    public void setPk(ListReferenceId pk) {
        this.pk = pk;
    }

    /**
     * Obtient la liste
     *
     * @return Liste
     */
    @Transient
    public List getList() {
        return getPk().getList();
    }

    /**
     * Définit la liste
     *
     * @param list Liste
     */
    public void setList(List list) {
        getPk().setList(list);
    }

    /**
     * Obtient la référence
     *
     * @return Référence
     */
    @Transient
    public Reference getReference() {
        return getPk().getReference();
    }

    /**
     * Définit la référence
     *
     * @param reference
     */
    public void setReference(Reference reference) {
        getPk().setReference(reference);
    }

    /**
     * Définit la quantité de la référence dans la liste
     *
     * @return Quantité
     */
    @Column(name = "quantity", nullable = false)
    public int getQuantity() {
        return quantity;
    }

    /**
     * Définit la quantité de la référence dans la liste
     *
     * @param quantity Quantité
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ListReference that = (ListReference) o;

        if (getPk() != null ? !getPk().equals(that.getPk())
                : that.getPk() != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return (getPk() != null ? getPk().hashCode() : 0);
    }
}
