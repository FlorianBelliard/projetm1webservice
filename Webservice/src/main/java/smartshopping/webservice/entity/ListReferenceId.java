/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.entity;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

/**
 * Classe réprésentant la clé primaire de la calsse ListReference
 *
 * @author FLORIAN
 */
@Embeddable
public class ListReferenceId extends Entity implements Serializable {

    private List list;

    private Reference reference;

    @ManyToOne
    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @ManyToOne
    public Reference getReference() {
        return reference;
    }

    public void setReference(Reference reference) {
        this.reference = reference;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ListReferenceId that = (ListReferenceId) o;

        if (list != null ? !list.equals(that.list) : that.list != null) {
            return false;
        }
        if (reference != null ? !reference.equals(that.reference) : that.reference != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = (list != null ? list.hashCode() : 0);
        result = 31 * result + (reference != null ? reference.hashCode() : 0);
        return result;
    }
}
