/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/**
 * Entité abstraite représentant une personne
 * @author FLORIAN
 */
@javax.persistence.Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Person extends smartshopping.webservice.entity.Entity implements Serializable {

    /**
     * Identifiant de l'utilisateur
     */
    protected long id;

    /**
     * Login de l'utilisateur
     */
    protected String login;

    /**
     * Mot de passe de l'utilisateur
     */
    protected String password;

    /**
     * Obtient l'identifiant de l'utilisateur
     *
     * @return Identifiant
     */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "id", nullable = false, unique = true)
    public long getId() {
        return id;
    }

    /**
     * Définit l'identifiant de l'utilisateur
     *
     * @param id Identifiant
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Obtient le login de l'utilisateur
     *
     * @return Login
     */
    @Column(name = "login", nullable = false, unique = true, length = 50)
    public String getLogin() {
        return login;
    }

    /**
     * Définit le login de l'utilisateur
     *
     * @param login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Obtient le mot de passe de l'utilisateur
     *
     * @return Mot de passe
     */
    @Column(name = "password", nullable = false, length = 50)
    public String getPassword() {
        return password;
    }

    /**
     * Définit le mot de passe de l'utilisateur
     *
     * @param password Mot de passe
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
