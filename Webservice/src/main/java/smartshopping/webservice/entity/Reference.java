/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Classe réprésentant une référence de produit
 *
 * @author FLORIAN
 */
@Entity
@Table(name = "reference", catalog = "smartshopping")
public class Reference extends smartshopping.webservice.entity.Entity implements Serializable {

    /**
     * Identifiant de la référence
     */
    private long id;

    /**
     * Code de la référence
     */
    private String code;

    /**
     * Libellé de la référence
     */
    private String label;

    /**
     * Marque associée à la référence
     */
    private Brand brand;

    /**
     * Liste des articles correpondant à cette réference
     */
    private Set<Item> items = new HashSet<Item>(0);

    /**
     * Liste des produits correspondant à cette référence
     */
    private Set<ListReference> listReferences = new HashSet<ListReference>(0);

    /**
     * Constructeur par défaut
     */
    public Reference() {
    }

    /**
     * Constructeur avec paramètres
     *
     * @param code Code
     * @param label Libellé
     * @param brand Marque
     * @param items Liste des articles
     * @param listReferences Liste des produits
     */
    public Reference(String code, String label, Brand brand, Set<Item> items, Set<ListReference> listReferences) {
        this.code = code;
        this.label = label;
        this.brand = brand;
        this.items = items;
        this.listReferences = listReferences;
    }

    /**
     * Obtient l'idenfiant de la référence
     *
     * @return Identifiant
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    public long getId() {
        return id;
    }

    /**
     * Définit l'identifiant
     *
     * @param id Identifiant
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Obtient le code de la référence
     *
     * @return Code de la référence
     */
    @Column(name = "code", unique = true, nullable = false)
    public String getCode() {
        return code;
    }

    /**
     * Définit le code de la référence
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Obtient le libellé de la référence
     *
     * @return Libellé
     */
    @Column(name = "label", nullable = false, length = 50)
    public String getLabel() {
        return label;
    }

    /**
     * Définit le libellé de la référence
     *
     * @param label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Obtien la marque associée à la référence
     *
     * @return Marque
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "brandid", nullable = false)
    public Brand getBrand() {
        return brand;
    }

    /**
     * Définit la marque associée à la référence
     *
     * @param brand Marque
     */
    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    /**
     * Obtient la liste des articles correspondant à la référence
     *
     * @return Liste des articles
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "reference")
    public Set<Item> getItems() {
        return items;
    }

    /**
     * Définit la liste des articles correspondant à la référence
     *
     * @param items
     */
    public void setItems(Set<Item> items) {
        this.items = items;
    }

    /**
     * Obtient la liste des produits correspondant à cette référence
     *
     * @return
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.reference")
    public Set<ListReference> getListReferences() {
        return listReferences;
    }

    /**
     * Définit la liste des produits correspondant à cette référence
     *
     * @param listReferences
     */
    public void setListReferences(Set<ListReference> listReferences) {
        this.listReferences = listReferences;
    }

}
