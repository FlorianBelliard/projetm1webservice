/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 * Classe réprésentant les courses à effectuer
 *
 * @author FLORIAN
 */
@Entity
@Table(name = "shopping", catalog = "smartshopping")
public class Shopping extends smartshopping.webservice.entity.Entity implements Serializable {

    /**
     * Identifiant des courses
     */
    private long id;

    /**
     * Statut des courses
     */
    private Boolean status;

    /**
     * Prix des courses
     */
    private double price;

    /**
     * Date des courses
     */
    private Date date;

    /**
     * Liste associée aux courses
     */
    private List list;

    /**
     * Liste des articles présents dans la liste de courses
     */
    private Set<ShoppingItem> shoppingItems = new HashSet<ShoppingItem>(0);

    /**
     * Magasin dans lequel sont effectués les courses
     */
    private Store store;
    /**
     * Constructeur par défaut
     */
    public Shopping() {
    }

    /**
     * Constructeur avec paramètres
     *
     * @param id Identifiant
     * @param status Statut
     * @param date Date
     * @param price Prix
     * @param list Liste
     */
    public Shopping(long id, Boolean status, Date date, double price, List list, Set<ShoppingItem> shoppingItems) {
        this.id = id;
        this.status = status;
        this.date = date;
        this.price = price;
        this.list = list;
        this.shoppingItems = shoppingItems;
    }

    /**
     * Obtient l'identifiant des courses
     *
     * @return Identifiant
     */
    @Id
    @Column(name = "listid", unique = true, nullable = false)
    @GeneratedValue(generator = "gen")
    @GenericGenerator(name = "gen", strategy = "foreign", parameters = @Parameter(name = "property", value = "list"))
    public long getId() {
        return id;
    }

    /**
     * Définit l'identifiant des courses
     *
     * @param id Identifiant
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Obtient le statut des courses
     *
     * @return Statut
     */
    @Column(name = "status", nullable = false)
    public Boolean getStatus() {
        return status;
    }

    /**
     * Définit le statut des courses
     *
     * @param status Statut
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * Obtient la date des courses
     *
     * @return Date des courses
     */
    @Temporal(TemporalType.DATE)
    @Column(name = "date", nullable = false)
    public Date getDate() {
        return date;
    }

    /**
     * Définit la date des courses
     *
     * @param date Date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Obtient le prix des courses
     *
     * @return Prix
     */
    @Column(name = "price", nullable = false, precision = 2)
    public double getPrice() {
        return price;
    }

    /**
     * Définit le prix des courses
     *
     * @param price Prix
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * Obtient la liste auquelle correspond les courses
     *
     * @return Liste
     */
    @OneToOne(fetch = FetchType.EAGER)
    @PrimaryKeyJoinColumn
    public List getList() {
        return list;
    }

    /**
     * Définit la liste auquelle correspond les courses
     *
     * @param list Liste
     */
    public void setList(List list) {
        this.list = list;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.shopping")
    public Set<ShoppingItem> getShoppingItems() {
        return shoppingItems;
    }

    public void setShoppingItems(Set<ShoppingItem> shoppingItems) {
        this.shoppingItems = shoppingItems;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "storeid", nullable = false)
    public Store getStore() {
        return store; 
    }

    public void setStore(Store store) {
        this.store = store;
    }

}
