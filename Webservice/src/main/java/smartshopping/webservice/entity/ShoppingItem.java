/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.entity;

import java.io.Serializable;
import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Classe réprésentant le fait qu'un article soit dans une course
 *
 * @author FLORIAN
 */
@Entity
@Table(name = "shopping_item", catalog = "smartshopping")
@AssociationOverrides({
    @AssociationOverride(name = "pk.shopping",
            joinColumns = @JoinColumn(name = "shoppingid")),
    @AssociationOverride(name = "pk.item",
            joinColumns = @JoinColumn(name = "itemid"))})
public class ShoppingItem extends smartshopping.webservice.entity.Entity implements Serializable {

    /**
     * Clé primaire de la classe ShoppingItem
     */
    private ShoppingItemId pk = new ShoppingItemId();

    /**
     * Quantité de l'article dans les courses
     */
    private int quantity;

    /**
     * Statut de l'article dans les courses
     */
    private Boolean status;

    /**
     * Constructeur par défaut
     */
    public ShoppingItem() {
    }

    public ShoppingItem(Shopping shopping, Item item, int quantity, Boolean status) {
        this.pk.setShopping(shopping);
        this.pk.setItem(item);
        this.quantity = quantity;
        this.status = status;
    }

    /**
     * Obtient la clé primaire
     *
     * @return Clé primaire
     */
    @EmbeddedId
    public ShoppingItemId getPk() {
        return pk;
    }

    /**
     * Définit la clé primaire
     *
     * @param pk Clé primaire
     */
    public void setPk(ShoppingItemId pk) {
        this.pk = pk;
    }

    /**
     * Obtient les courses
     *
     * @return Courses
     */
    @Transient
    public Shopping getShopping() {
        return getPk().getShopping();
    }

    /**
     * Définit les courses
     *
     * @param shopping Courses
     */
    public void setShopping(Shopping shopping) {
        getPk().setShopping(shopping);
    }

    /**
     * Obtient l'article
     *
     * @return Article
     */
    @Transient
    public Item getItem() {
        return getPk().getItem();
    }

    /**
     * Définit l'article
     *
     * @param item Article
     */
    public void setItem(Item item) {
        getPk().setItem(item);
    }

    /**
     * Obtient la quantité de l'article dans les courses
     *
     * @return Quantité
     */
    @Column(name = "quantity", nullable = false)
    public int getQuantity() {
        return quantity;
    }

    /**
     * Définit la quantité de l'article dans les courses
     *
     * @param quantity Quantité
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * Obtient le statut d'un article dans la liste de courses
     *
     * @return Statut
     */
    @Column(name = "status", nullable = false)
    public Boolean getStatus() {
        return status;
    }

    /**
     * Définit le statut d'un article dans la liste des courses
     *
     * @param status Statut
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShoppingItem that = (ShoppingItem) o;

        if (getPk() != null ? !getPk().equals(that.getPk())
                : that.getPk() != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return (getPk() != null ? getPk().hashCode() : 0);
    }
}
