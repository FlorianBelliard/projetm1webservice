/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.entity;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

/**
 * Classe réprésentant la clé primaire de la classe ShoppingItem
 *
 * @author FLORIAN
 */
@Embeddable
public class ShoppingItemId extends Entity implements Serializable {

    private Shopping shopping;

    private Item item;

    @ManyToOne
    public Shopping getShopping() {
        return shopping;
    }

    public void setShopping(Shopping shopping) {
        this.shopping = shopping;
    }

    @ManyToOne
    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShoppingItemId that = (ShoppingItemId) o;

        if (shopping != null ? !shopping.equals(that.shopping) : that.shopping != null) {
            return false;
        }
        if (item != null ? !item.equals(that.item) : that.item != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = (shopping != null ? shopping.hashCode() : 0);
        result = 31 * result + (item != null ? item.hashCode() : 0);
        return result;
    }

}
