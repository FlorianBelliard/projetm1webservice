/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Classe représentant un magasin
 *
 * @author FLORIAN
 */
@Entity
@Table(name = "store", catalog = "smartshopping")
public class Store extends smartshopping.webservice.entity.Entity implements Serializable {

    /**
     * Identifiant du magasin
     */
    private long id;

    /**
     * Libellé du magasin
     */
    private String label;

    /**
     * Adresse du magasin
     */
    private String address;

    /**
     * Position GPS du magasin
     */
    private String position;

    /**
     * Chaîne auquelle appartient le magasin
     */
    private Chain chain;

    /**
     * Liste des articles du magasin
     */
    private Set<Item> items = new HashSet<Item>(0);

    /**
     * Liste des utilisateurs qui ont ce magasin pour favoris
     */
    private Set<User> users = new HashSet<User>(0);
    
    /**
     * Liste des courses effectuées dans ce magasin
     */
    private Set<Shopping> shoppings = new HashSet<Shopping>(0);

    /**
     * Constructeur par défaut
     */
    public Store() {
    }

    /**
     * Constructeur avec paramètres
     *
     * @param id Identifiant
     * @param label Libellé
     * @param address Adresse
     * @param position Position
     * @param items Liste des articles
     * @param users Utilisateurs
     */
    public Store(long id, String label, String address, String position, Set<Item> items, Set<User> users) {
        this.id = id;
        this.label = label;
        this.address = address;
        this.position = position;
        this.items = items;
        this.users = users;
    }

    /**
     * Obtient l'identifiant du magasin
     *
     * @return Identifiant
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public long getId() {
        return id;
    }

    /**
     * Définit l'identifiant du magasin
     *
     * @param id Identifiant
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Obtient le libellé du magasin
     *
     * @return Libellé
     */
    @Column(name = "label", nullable = false, length = 50)
    public String getLabel() {
        return label;
    }

    /**
     * Définit le libellé du magasin
     *
     * @param label Libellé
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Obtient l'adresse du magasin
     *
     * @return Adresse
     */
    @Column(name = "address", nullable = false, length = 50)
    public String getAddress() {
        return address;
    }

    /**
     * Définit l'adresse du magasin
     *
     * @param address Adresse
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Obtient la position du magasin
     *
     * @return Position
     */
    @Column(name = "position", nullable = false, length = 50)
    public String getPosition() {
        return position;
    }

    /**
     * Définit la position du magasin
     *
     * @param position Position
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * Obtient la chaîne auquelle appartient le magasin
     *
     * @return Chaîne
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "chainid", nullable = false)
    public Chain getChain() {
        return chain;
    }

    /**
     * Définit la chaîne auquelle appartient le magasin
     *
     * @param chain Chaine
     */
    public void setChain(Chain chain) {
        this.chain = chain;
    }

    /**
     * Obtient la liste des articles du magasins
     *
     * @return Liste des articles
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "store")
    public Set<Item> getItems() {
        return items;
    }

    /**
     * Définit la liste des articles du magasin
     *
     * @param items Liste des articles
     */
    public void setItems(Set<Item> items) {
        this.items = items;
    }

    /**
     * Obtient la liste des utilisateurs qui ont ce magasin en favori
     *
     * @return Liste des utilisateurs
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "user_store", catalog = "smartshopping", joinColumns = {
        @JoinColumn(name = "storeid", nullable = false, updatable = false)},
            inverseJoinColumns = {
                @JoinColumn(name = "userid",
                        nullable = false, updatable = false)})
    public Set<User> getUsers() {
        return users;
    }

    /**
     * Définit la liste des utilisateurs qui ont ce magasin en favori
     *
     * @param users Liste des utilisateurs
     */
    public void setUsers(Set<User> users) {
        this.users = users;
    }

    /**
     * Obtient la liste des courses effectuées dans ce magasin
     * @return Liste des courses effectuées
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "store")
    public Set<Shopping> getShoppings() {
        return shoppings;
    }

    /**
     * Définit la liste des courses effectuées dans un magasin
     * @param shoppings Liste des courses effectuées
     */
    public void setShoppings(Set<Shopping> shoppings) {
        this.shoppings = shoppings;
    }
}
