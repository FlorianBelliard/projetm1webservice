/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Classe réprésentant un utilisateur
 *
 * @author FLORIAN
 */
@Entity
@Table(name = "user", catalog = "smartshopping")
public class User extends Person {

    /**
     * Nom de l'utilisateur
     */
    private String lastName;

    /**
     * Prénom de l'utilisateur
     */
    private String firstName;

    /**
     * Adresse de l'utilisateur
     */
    private String address;

    /**
     * Liste des listes de l'utilisateur
     */
    private Set<List> lists = new HashSet<List>(0);

    /**
     * Liste des magasins favoris de l'utilisateur
     */
    private Set<Store> favoritesStores = new HashSet<Store>(0);

    /**
     * Constructeur par défaut
     */
    public User() {
    }

    /**
     * Constructeur avec paramètres
     *
     * @param id Identifiant
     * @param lastName Nom
     * @param firstName Prénom
     * @param address Adresse
     * @param login Login
     * @param password Mot de passe
     * @param lists Listes
     * @param favoriteStores Magasins favoris
     */
    public User(long id, String lastName, String firstName, String address, String login, String password, Set<List> lists, Set<Store> favoriteStores) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.address = address;
        this.login = login;
        this.password = password;
        this.favoritesStores = favoriteStores;
        this.lists = lists;
    }

    /**
     * Obtient le nom de l'utilisateur
     *
     * @return Nom
     */
    @Column(name = "lastname", nullable = false, length = 50)
    public String getLastName() {
        return lastName;
    }

    /**
     * Définit le nom de l'utiliasteur
     *
     * @param lastName Nom
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Obtient le prénom de l'utilisateur
     *
     * @return Prénom
     */
    @Column(name = "firstname", nullable = false, length = 50)
    public String getFirstName() {
        return firstName;
    }

    /**
     * Définit le nom de l'utilisateur
     *
     * @param firstName Prénom
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Obtient l'adresse de l'utilisateur
     *
     * @return Adresse
     */
    @Column(name = "address", nullable = false, length = 50)
    public String getAddress() {
        return address;
    }

    /**
     * Définit l'adresse de l'utilisateur
     *
     * @param address Adresse
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Obtient la listes des listes de l'utilisateur
     *
     * @return Liste des listes
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    public Set<List> getLists() {
        return lists;
    }

    /**
     * Définit la liste des listes de l'utilisateur
     *
     * @param lists Liste des listes
     */
    public void setLists(Set<List> lists) {
        this.lists = lists;
    }

    /**
     * Obtient la liste des magasins favoris de l'utilisateur
     *
     * @return Liste des magasins
     */
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "users")
    public Set<Store> getFavoritesStores() {
        return favoritesStores;
    }

    /**
     * Définti la liste des magasins favoris de l'utilisateur
     *
     * @param favoritesStores Liste des magasins
     */
    public void setFavoritesStores(Set<Store> favoritesStores) {
        this.favoritesStores = favoritesStores;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof User)) {
            return false;
        }
        return this.id == ((User) obj).getId();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + (this.lastName != null ? this.lastName.hashCode() : 0);
        hash = 43 * hash + (this.firstName != null ? this.firstName.hashCode() : 0);
        return hash;
    }

}
