/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

/**
 *
 * @author FLORIAN
 */
public class MappingException extends WebApplicationException {

    public MappingException() {
        super(Response.status(Response.Status.INTERNAL_SERVER_ERROR).build());
    }

    public MappingException(String message) {
        super(Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(message).build());
    }
}
