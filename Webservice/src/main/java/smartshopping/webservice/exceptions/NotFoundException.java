/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Exception indiquant que l'entité n'a pas été trouvée
 *
 * @author FLORIAN
 */
public class NotFoundException extends WebApplicationException {

    public NotFoundException() {
        super(Response.status(Response.Status.NOT_FOUND).build());
    }

    public NotFoundException(Class type, long id) {
        super(Response.status(Response.Status.NOT_FOUND).
                entity("L'entité de type " + type.getSimpleName() + " et d'identifiant " + id + " n'a pas été trouvée.").
                type(MediaType.TEXT_PLAIN + "; charset=utf-8").
                build());
    }

    public NotFoundException(Class type, long id1, long id2) {
        super(Response.status(Response.Status.NOT_FOUND).
                entity("L'entité de type " + type.getSimpleName() + " et d'identifiants " + id1 + " et " + id2 + " n'a pas été trouvée.").
                type(MediaType.TEXT_PLAIN + "; charset=utf-8").
                build());
    }
}
