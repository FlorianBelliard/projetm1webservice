/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.mapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.dozer.DozerBeanMapper;
import org.dozer.MappingException;
import smartshopping.webservice.entity.Entity;
import smartshopping.webservicelib.model.DTO;

/**
 * Permet de transformer une entité Hibernate en DTO (Data Transfer Object) pour
 * faciliter son transport dans les flux réseaux
 *
 * @author FLORIAN
 * @param <S> DTO
 * @param <T> Entité
 */
public class DozerMapper<T extends Entity, S extends DTO> {

    /**
     * Répertoire contenant les fichiers spécifiant les mappings
     */
    private static final String mappingDirectory = "dozerMapping/";

    /**
     * Nom des fichiers spécifiants les mappings
     */
    private static final List<String> mappingFiles = new ArrayList<String>(Arrays.asList(
            mappingDirectory + "BrandMapping.xml",
            mappingDirectory + "ChainMapping.xml",
            mappingDirectory + "ItemMapping.xml",
            mappingDirectory + "ListMapping.xml",
            mappingDirectory + "ListReferenceMapping.xml",
            mappingDirectory + "ReferenceMapping.xml",
            mappingDirectory + "ShoppingItemMapping.xml",
            mappingDirectory + "ShoppingMapping.xml",
            mappingDirectory + "StoreMapping.xml",
            mappingDirectory + "UserMapping.xml",
            mappingDirectory + "AdminMapping.xml"));

    /**
     * Mapper de Dozer
     */
    private static final DozerBeanMapper mapper = new DozerBeanMapper(mappingFiles);

    /**
     * Type de l'entité Hibernate
     */
    private final Class<T> entityType;

    /**
     * Type du DTO
     */
    private final Class<S> dtoType;

    /**
     * Constructeur avec les deux types en paramètres
     *
     * @param entityType Type de l'entité Hibernate
     * @param dtoType Type du DTO
     */
    public DozerMapper(Class<T> entityType, Class<S> dtoType) {
        this.entityType = entityType;
        this.dtoType = dtoType;
    }

    /**
     * Transforme une entité Hibernate en DTO
     *
     * @param entity Entité
     * @param dtoType Type du DTO
     * @return DTO
     * @throws MappingException
     */
    public S mapEntityToDTO(T entity, Class<S> dtoType) throws MappingException {
        return mapper.map(entity, dtoType);
    }

    /**
     * Transforme un DTO en l'entité Hibernate correspondante
     *
     * @param dto DTO
     * @param entity Entité
     * @throws MappingException
     */
    public void mapDTOToEntity(S dto, Entity entity) throws MappingException {
        mapper.map(dto, entity);
    }

    /**
     * Transforme un DTO en une entité Hibernate
     *
     * @param dto DTO
     * @param entityType Type de l'entité HIbernate
     * @return Entité
     * @throws MappingException
     */
    public T mapDTOToEntity(S dto, Class<T> entityType) throws MappingException {
        return mapper.map(dto, entityType);
    }

    /**
     * Transforme une liste d'entités en une liste de DTOs
     *
     * @param entities Liste d'entités
     * @param dtoType Type des DTOs
     * @return Liste des DTOs
     * @throws MappingException
     */
    public List<S> mapEntitiesToDTOs(List<T> entities, Class<S> dtoType) throws MappingException {
        List<S> dtos = new ArrayList<S>();
        for (T entity : entities) {
            dtos.add(mapper.map(entity, dtoType));
        }
        return dtos;
    }

    /**
     * Transforme une liste de DTOs en une liste d'entités
     *
     * @param dtos Liste des DTOs
     * @param entities Liste des entités
     * @throws MappingException
     */
    public void mapDTOsToEntities(List<S> dtos, List<T> entities) throws MappingException {
        for (int i = 0; i < dtos.size(); i++) {
            mapper.map(dtos.get(i), entities.get(i));
        }
    }
}
