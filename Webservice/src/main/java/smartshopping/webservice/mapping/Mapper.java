/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.mapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.dozer.MappingException;
import smartshopping.webservice.dao.GenericDAO;
import smartshopping.webservice.dao.interfaces.IAssociationDAO;
import smartshopping.webservice.entity.Admin;
import smartshopping.webservice.entity.Brand;
import smartshopping.webservice.entity.Chain;
import smartshopping.webservice.entity.Entity;
import smartshopping.webservice.entity.Item;
import smartshopping.webservice.entity.List;
import smartshopping.webservice.entity.ListReference;
import smartshopping.webservice.entity.Reference;
import smartshopping.webservice.entity.Shopping;
import smartshopping.webservice.entity.ShoppingItem;
import smartshopping.webservice.entity.Store;
import smartshopping.webservice.entity.User;
import smartshopping.webservicelib.exceptions.SerializationException;
import smartshopping.webservicelib.model.AdminDTO;
import smartshopping.webservicelib.model.BrandDTO;
import smartshopping.webservicelib.model.ChainDTO;
import smartshopping.webservicelib.model.DTO;
import smartshopping.webservicelib.model.ItemDTO;
import smartshopping.webservicelib.model.ListDTO;
import smartshopping.webservicelib.model.ListReferenceDTO;
import smartshopping.webservicelib.model.ReferenceDTO;
import smartshopping.webservicelib.model.ShoppingDTO;
import smartshopping.webservicelib.model.ShoppingItemDTO;
import smartshopping.webservicelib.model.StoreDTO;
import smartshopping.webservicelib.model.UserDTO;
import smartshopping.webservicelib.model.interfaces.IAssociationDTO;
import smartshopping.webservicelib.serialization.JsonObjectMapper;

/**
 * Classe qui simplifie la transformation d'une entité Hibernate en chaîne au
 * format JSON en masquant la transformation en DTO effectuée par Dozer, ainsi
 * que la sérialisation réalisée par Jackson
 *
 * @author FLORIAN
 * @param <T>
 */
public class Mapper<T extends Entity> {

    /**
     * Sérialiseur JSON
     */
    private JsonObjectMapper jsonMapper;

    /**
     * Mapper Dozer
     */
    private DozerMapper dozerMapper;

    /**
     * Type de l'entité Hibernate
     */
    private final Class<T> type;

    /**
     * DAO correspondant à l'entité
     */
    private GenericDAO<T> dao;

    /**
     * Map qui contient les couples Entité/DTO
     */
    private static final Map<Class<? extends Entity>, Class<? extends DTO>> dtoMapper = new HashMap<Class<? extends Entity>, Class<? extends DTO>>();

    static {
        dtoMapper.put(Brand.class, BrandDTO.class);
        dtoMapper.put(Chain.class, ChainDTO.class);
        dtoMapper.put(Item.class, ItemDTO.class);
        dtoMapper.put(List.class, ListDTO.class);
        dtoMapper.put(ListReference.class, ListReferenceDTO.class);
        dtoMapper.put(Reference.class, ReferenceDTO.class);
        dtoMapper.put(Shopping.class, ShoppingDTO.class);
        dtoMapper.put(ShoppingItem.class, ShoppingItemDTO.class);
        dtoMapper.put(Store.class, StoreDTO.class);
        dtoMapper.put(User.class, UserDTO.class);
        dtoMapper.put(Admin.class, AdminDTO.class);
    }

    /**
     * Constructeur
     *
     * @param type Type de l'entité
     * @param dao DAO de l'entité
     */
    public Mapper(Class<T> type, GenericDAO<T> dao) {
        this.type = type;
        jsonMapper = new JsonObjectMapper(dtoMapper.get(type));
        dozerMapper = new DozerMapper(type, dtoMapper.get(type));
        this.dao = dao;
    }

    /**
     * Transforme une chaîne au format JSON en une entité Hibernate
     *
     * @param serialized Chaîne au format JSON
     * @return Entité
     */
    public T fromDataToEntity(String serialized) {
        T entity = null;
        DTO dto = null;
        try {
            dto = (DTO) jsonMapper.read(serialized);

            if (IAssociationDAO.class.isAssignableFrom(dao.getClass())) {
                entity = (T) ((IAssociationDAO) dao).getByIds(((IAssociationDTO) dto).getId1(), ((IAssociationDTO) dto).getId2());
            } else {
                entity = dao.getById(type, dto.getId());
            }

            if (entity == null) {
                entity = (T) dozerMapper.mapDTOToEntity(dto, type);
            } else {
                dozerMapper.mapDTOToEntity(dto, entity);
            }
        } catch (SerializationException ex) {
            Logger.getLogger(Mapper.class.getName()).log(Level.SEVERE, null, ex);
            throw new MappingException(ex.getLocalizedMessage());
        } catch (MappingException ex) {
            Logger.getLogger(Mapper.class.getName()).log(Level.SEVERE, null, ex);
            throw new MappingException(ex.getLocalizedMessage());
        }
        return entity;
    }

    /**
     * Transforme une chaîne au format JSON en une liste d'entités
     *
     * @param serialized Chaîne au format JSON
     * @return Liste d'entités
     */
    public java.util.List<T> fromDatasToEntities(String serialized) {
        java.util.List<T> entities = new ArrayList<T>();
        java.util.List<DTO> dtos = null;
        try {
            dtos = jsonMapper.readList(serialized);

            for (DTO dto : dtos) {
                T entity = null;
                if (IAssociationDAO.class.isAssignableFrom(dao.getClass())) {
                    entity = (T) ((IAssociationDAO) dao).getByIds(((IAssociationDTO) dto).getId1(), ((IAssociationDTO) dto).getId2());
                } else {
                    entity = dao.getById(type, dto.getId());
                }

                if (entity == null) {
                    entity = type.newInstance();
                }
                entities.add(entity);
            }
            dozerMapper.mapDTOsToEntities(dtos, entities);

        } catch (SerializationException ex) {
            Logger.getLogger(Mapper.class.getName()).log(Level.SEVERE, null, ex);
            throw new smartshopping.webservice.exceptions.MappingException(ex.getLocalizedMessage());
        } catch (MappingException ex) {
            Logger.getLogger(Mapper.class.getName()).log(Level.SEVERE, null, ex);
            throw new smartshopping.webservice.exceptions.MappingException(ex.getLocalizedMessage());
        } catch (InstantiationException ex) {
            Logger.getLogger(Mapper.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(Mapper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return entities;
    }

    /**
     * Transforme une entité Hibernate en chaîne au format JSON
     *
     * @param entity Entité
     * @return Chaîne au format JSON
     */
    public String fromEntityToData(T entity) {
        String serialized = null;
        DTO dto = null;
        try {
            dto = dozerMapper.mapEntityToDTO(entity, dtoMapper.get(type));
            serialized = jsonMapper.write(dto);
        } catch (SerializationException ex) {
            Logger.getLogger(Mapper.class.getName()).log(Level.SEVERE, null, ex);
            throw new smartshopping.webservice.exceptions.MappingException(ex.getLocalizedMessage());
        } catch (MappingException ex) {
            Logger.getLogger(Mapper.class.getName()).log(Level.SEVERE, null, ex);
            throw new smartshopping.webservice.exceptions.MappingException(ex.getLocalizedMessage());
        }
        return serialized;
    }

    /**
     * Transforme une liste d'entités en chaîne au format JSON
     *
     * @param entities Liste des entités
     * @return Chaîne au format JSON
     */
    public String fromEntitiesToDatas(java.util.List<T> entities) {
        String serialized = null;
        java.util.List<DTO> dtos = null;

        try {
            dtos = dozerMapper.mapEntitiesToDTOs(entities, dtoMapper.get(type));
            serialized = jsonMapper.writeList(dtos);
        } catch (SerializationException ex) {
            Logger.getLogger(Mapper.class.getName()).log(Level.SEVERE, null, ex);
            throw new smartshopping.webservice.exceptions.MappingException(ex.getLocalizedMessage());
        } catch (MappingException ex) {
            Logger.getLogger(Mapper.class.getName()).log(Level.SEVERE, null, ex);
            throw new smartshopping.webservice.exceptions.MappingException(ex.getLocalizedMessage());
        }
        return serialized;
    }
}
