/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import smartshopping.webservice.dao.AdminDAO;
import smartshopping.webservice.entity.Admin;
import smartshopping.webservice.util.CustomResponse;

/**
 * Classe contenant les services liés aux administrateurs
 * @author FLORIAN
 */
@Path("/admins")
public class AdminService extends GenericService<Admin> {

    /**
     * Service indiquant si l'administrateur existe
     * @param login Login de l'administrateur
     * @param password Mot de passe de l'administrateur
     * @return Réponse HTTP contenant l'administrateur trouvé
     */
    @POST
    @Path("/exists")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response isAdminExist(@FormParam("login") String login, @FormParam("password") String password) {
        Admin admin = ((AdminDAO) getDAO()).isAdminExist(login, password);
        String adminSerialized = null;

        if (admin != null) {
            adminSerialized = getMapper().fromEntityToData(admin);
        } else {
            adminSerialized = null;
        }

        return CustomResponse.found(adminSerialized);
    }
}
