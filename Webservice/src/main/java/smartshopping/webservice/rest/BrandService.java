/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.rest;

import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import smartshopping.webservice.dao.ChainDAO;
import smartshopping.webservice.dao.ReferenceDAO;
import smartshopping.webservice.entity.Brand;
import smartshopping.webservice.entity.Chain;
import smartshopping.webservice.entity.Reference;
import smartshopping.webservice.mapping.Mapper;
import smartshopping.webservice.util.CustomResponse;

/**
 * Classe contenant les services liés aux marques
 *
 * @author FLORIAN
 */
@Path("/brands")
public class BrandService extends GenericService<Brand> {

    /**
     * Service retournant la liste des chaînes où la marque est présente
     *
     * @param id Identifiant de la marque
     * @return Réponse HTTP contenant la liste des chaînes
     */
    @GET
    @Path("/{id}/chains/")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response getChainsOfBrand(@PathParam("id") long id) {
        List<Chain> chains = ((ChainDAO) getDAOByType(Chain.class)).getChainsForBrand(id);
        Mapper<Chain> mapper = new Mapper<Chain>(Chain.class, null);
        return CustomResponse.found(mapper.fromEntitiesToDatas(chains));
    }

    /**
     * Service retournant la liste des références ayant cette marque
     *
     * @param id Identifiant de la marque
     * @return Réponse HTTP contenant la liste des références
     */
    @GET
    @Path("/{id}/references/")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response getReferencesOfBrand(@PathParam("id") long id) {
        List<Reference> references = ((ReferenceDAO) getDAOByType(Reference.class)).getReferencesForBrand(id);
        Mapper<Reference> mapper = new Mapper<Reference>(Reference.class, null);
        return CustomResponse.found(mapper.fromEntitiesToDatas(references));
    }
}
