/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.rest;

import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import smartshopping.webservice.dao.BrandDAO;
import smartshopping.webservice.dao.ChainDAO;
import smartshopping.webservice.dao.StoreDAO;
import smartshopping.webservice.entity.Brand;
import smartshopping.webservice.entity.Chain;
import smartshopping.webservice.entity.Store;
import smartshopping.webservice.mapping.Mapper;
import smartshopping.webservice.util.CustomResponse;

/**
 * Classe contenant les services liés aux chaînes
 * @author FLORIAN
 */
@Path("/chains")
public class ChainService extends GenericService<Chain> {

    /**
     * Service retournant la liste des marques d'une chaîne
     * @param id Identfiant de la chaîne
     * @return Réponse HTTP contenant la liste des marques
     */
    @GET
    @Path("/{id}/brands/")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response getBrandsOfChain(@PathParam("id") long id) {
        List<Brand> brands = ((BrandDAO) getDAOByType(Brand.class)).getBrandsForChain(id);
        Mapper<Brand> mapper = new Mapper<Brand>(Brand.class, null);
        return CustomResponse.found(mapper.fromEntitiesToDatas(brands));
    }

    /**
     * Service retournant la liste des magasins appartenant à la chaîne
     * @param id Identifiant de la chaîne
     * @return Réponse HTTP contenant la liste des magasins
     */
    @GET
    @Path("/{id}/stores/")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response getStoresOfChain(@PathParam("id") long id) {
        List<Store> stores = ((StoreDAO) getDAOByType(Store.class)).getStoresForChain(id);
        Mapper<Store> mapper = new Mapper<Store>(Store.class, null);
        return CustomResponse.found(mapper.fromEntitiesToDatas(stores));
    }
}
