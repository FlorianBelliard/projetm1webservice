/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.rest;

import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import smartshopping.webservice.dao.AdminDAO;
import smartshopping.webservice.dao.BrandDAO;
import smartshopping.webservice.dao.ChainDAO;
import smartshopping.webservice.dao.GenericDAO;
import smartshopping.webservice.dao.ItemDAO;
import smartshopping.webservice.dao.ListDAO;
import smartshopping.webservice.dao.ListReferenceDAO;
import smartshopping.webservice.dao.ReferenceDAO;
import smartshopping.webservice.dao.ShoppingDAO;
import smartshopping.webservice.dao.ShoppingItemDAO;
import smartshopping.webservice.dao.StoreDAO;
import smartshopping.webservice.dao.UserDAO;
import smartshopping.webservice.entity.Admin;
import smartshopping.webservice.entity.Brand;
import smartshopping.webservice.entity.Chain;
import smartshopping.webservice.entity.Entity;
import smartshopping.webservice.entity.Item;
import smartshopping.webservice.entity.ListReference;
import smartshopping.webservice.entity.Reference;
import smartshopping.webservice.entity.Shopping;
import smartshopping.webservice.entity.ShoppingItem;
import smartshopping.webservice.entity.Store;
import smartshopping.webservice.entity.User;
import smartshopping.webservice.exceptions.NotFoundException;
import smartshopping.webservice.mapping.Mapper;
import smartshopping.webservice.util.CustomResponse;

/**
 * Service générique qui permet d'accéder aux informations de base pour toutes
 * les entités
 *
 * @author FLORIAN
 * @param <T> Entity
 */
public abstract class GenericService<T extends Entity> {

    /**
     * Classe à laquelle correspond le service
     */
    private Class<T> type;

    /**
     * Objet qui s'occupe de la sérialisation/désérialisation
     */
    private Mapper<T> mapper;

    /**
     * Map static faisant la correspondance entre la classe du service et le DAO
     * à utiliser. TODO: Voir si meilleur solution que d'utiliser les
     * constructeurs des DAO
     */
    private static final Map<Class<? extends Entity>, GenericDAO> daoMapper = new HashMap<Class<? extends Entity>, GenericDAO>();

    static {
        daoMapper.put(Brand.class, new BrandDAO());
        daoMapper.put(Chain.class, new ChainDAO());
        daoMapper.put(Item.class, new ItemDAO());
        daoMapper.put(smartshopping.webservice.entity.List.class, new ListDAO());
        daoMapper.put(ListReference.class, new ListReferenceDAO());
        daoMapper.put(Reference.class, new ReferenceDAO());
        daoMapper.put(Shopping.class, new ShoppingDAO());
        daoMapper.put(ShoppingItem.class, new ShoppingItemDAO());
        daoMapper.put(Store.class, new StoreDAO());
        daoMapper.put(User.class, new UserDAO());
        daoMapper.put(Admin.class, new AdminDAO());
    }

    /**
     * Constructeur générique du service qui va définir le type du service et
     * instancier le JsonObjectMapper
     */
    public GenericService() {
        this.type = ((Class) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0]);
        this.mapper = new Mapper<T>(type, getDAO());
    }

    /**
     * Retourne le type du service. Peut être utile dans les classes héritant de
     * celle-ci
     *
     * @return Type du service
     */
    protected Class<T> getType() {
        return type;
    }

    /**
     * Retourne l'objet gérant la sérialisation/désérialisation. Peut être utile
     * dans les classes héritant de celle-ci
     *
     * @return Objet gérant la sérialisation/désérialisation
     */
    protected Mapper<T> getMapper() {
        return mapper;
    }

    /**
     * Retourne le DAO correspondant au type du service. Peut être utile dans
     * les classes héritant de celle-ci
     *
     * @return DAO
     */
    protected GenericDAO getDAO() {
        return getDAOByType(this.type);
    }

    /**
     * Retourn le DAO correspondant au type passé en paramètre. Peut être utile
     * dans les classes héritant de celle-ci
     *
     * @param type Type
     * @return DAO
     */
    protected GenericDAO getDAOByType(Class type) {
        return daoMapper.get(type);
    }

    /**
     * Retourne l'objet correspondant à l'id passé en paramètre
     *
     * @param id Identifiant
     * @return Objet sérialisé
     */
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response getById(@PathParam("id") long id) {
        T o = (T) getDAO().getById(type, id);
        if (o == null) {
            throw new NotFoundException(type, id);
        }
        return CustomResponse.found(mapper.fromEntityToData(o));
    }

    /**
     * Retourne toute la liste des objets
     *
     * @return Liste des objets sérialisée
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response getAll() {
        List<T> o = getDAO().getAll(type);
        return CustomResponse.found(mapper.fromEntitiesToDatas(o));
    }

    /**
     * Ajoute un objet
     *
     * @param serialized Objet sérialisé
     * @return Message de confirmation
     */
    @POST
    public Response add(String serialized) {
        T o = (T) mapper.fromDataToEntity(serialized);
        getDAO().persist(o);
        return CustomResponse.created(mapper.fromEntityToData(o));
    }

    /**
     * Met à jour l'objet
     *
     * @param serialized Objet sérialisé
     * @return Message de confirmation
     */
    @PUT
    public Response update(String serialized) {
        T o = (T) mapper.fromDataToEntity(serialized);
        getDAO().merge(o);
        return CustomResponse.updated(mapper.fromEntityToData(o));
    }

    /**
     * Supprime un objet
     *
     * @param id Identifiant de l'objet
     * @return Message de confirmation
     */
    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") long id) {
        T o = (T) getDAO().getById(type, id);
        getDAO().delete(o);
        return CustomResponse.deleted(mapper.fromEntityToData(o));
    }

    /**
     * Cherche des résultats correspondant à la valeur et filtres fournis en
     * paramètre
     *
     * @param value Valeur recherchée
     * @param filters Filtres à appliquer à la recherche
     * @return Liste des résultats
     */
    @GET
    @Path("/search")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response search(@QueryParam("value") String value, @QueryParam("filter") List<String> filters) {
        List<T> results = getDAOByType(type).search(type, value, filters);
        return CustomResponse.found(mapper.fromEntitiesToDatas(results));
    }
}
