/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import smartshopping.webservice.entity.Item;
import smartshopping.webservice.entity.Reference;
import smartshopping.webservice.entity.Store;
import smartshopping.webservice.util.CustomResponse;

/**
 * Classe contenant le services liées aux produist d'un magasin
 * @author FLORIAN
 */
@Path("/items")
public class ItemService extends GenericService<Item> {

    /**
     * Service ajoutant un produit dans un magasin
     * @param serialized Chaine contenant le produit au format JSON
     * @param referenceid Identifiant de la référence correspondant au produit
     * @param storeid Identifiant du magasin dans lequel est présent le produit
     * @return Réponse HTTP contenant le produit ajouté
     */
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response add(@FormParam("serialized") String serialized, @FormParam("referenceid") long referenceid, @FormParam("storeid") long storeid) {
        Item item = (Item) getMapper().fromDataToEntity(serialized);
        Reference reference = (Reference) getDAOByType(Reference.class).getById(Reference.class, referenceid);
        Store store = (Store) getDAOByType(Store.class).getById(Store.class, storeid);

        item.setReference(reference);
        item.setStore(store);

        getDAO().persist(item);
        return CustomResponse.created(getMapper().fromEntityToData(item));
    }
}
