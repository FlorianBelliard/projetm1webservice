/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.rest;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;
import org.dozer.MappingException;
import smartshopping.webservice.dao.ListDAO;
import smartshopping.webservice.dao.ListReferenceDAO;
import smartshopping.webservice.dao.interfaces.IAssociationDAO;
import smartshopping.webservice.entity.List;
import smartshopping.webservice.entity.ListReference;
import smartshopping.webservice.entity.Reference;
import smartshopping.webservice.entity.User;
import smartshopping.webservice.exceptions.NotFoundException;
import smartshopping.webservice.mapping.DozerMapper;
import smartshopping.webservice.mapping.Mapper;
import smartshopping.webservice.util.CustomResponse;
import smartshopping.webservicelib.accessservice.EActionList;
import smartshopping.webservicelib.exceptions.SerializationException;
import smartshopping.webservicelib.model.ListReferenceDTO;
import smartshopping.webservicelib.serialization.JsonObjectMapper;

/**
 * Classe contenant les services liés aux listes
 *
 * @author FLORIAN
 */
public class ListService extends GenericService<List> {

    /**
     * Récupère toutes les listes d'un utilisateur
     *
     * @param userid Identifiant de l'utilisateur
     * @return Liste des listes
     */
    public Response getAllListsOfUser(long userid) {
        java.util.List<List> lists = ((ListDAO) getDAO()).getListsForUser(userid);
        Mapper<List> listMapper = new Mapper<List>(List.class, null);
        return CustomResponse.found(listMapper.fromEntitiesToDatas(lists));
    }

    /**
     * Récupère une liste d'un utilisateur
     *
     * @param listid Identifiant de la liste
     * @param userid Identifiant de la liste
     * @return Liste
     */
    public Response getListOfUser(long listid, long userid) {
        Mapper<List> listMapper = new Mapper<List>(List.class, null);
        List list = (List) ((ListDAO) getDAO()).getById(listid, userid);
        if (list == null) {
            throw new NotFoundException(List.class, listid);
        }
        return CustomResponse.found(listMapper.fromEntityToData(list));
    }

    /**
     * Ajoute une liste à un utilisateur
     *
     * @param userid Identifiant de l'utilisateur
     * @param serialized Chaîne contenant la liste au format JSON
     * @return Liste ajoutée
     */
    public Response addListToUser(long userid, String serialized) {
        Mapper<List> listMapper = new Mapper<List>(List.class, getDAO());
        List list = listMapper.fromDataToEntity(serialized);

        User user = (User) getDAOByType(User.class).getById(User.class, userid);
        list.setUser(user);

        getDAO().persist(list);
        return CustomResponse.created(listMapper.fromEntityToData(list));
    }

    /**
     * Supprime une liste d'un utilisateur
     *
     * @param listid Identifiant de la liste
     * @param userid Identfiant de l'utilisateur
     * @return Liste supprimée
     */
    public Response removeListFromUser(long listid, long userid) {
        List list = (List) ((ListDAO) getDAO()).getById(listid, userid);
        if (list == null) {
            throw new NotFoundException(List.class, listid);
        }
        getDAO().delete(list);

        Mapper<List> listMapper = new Mapper<List>(List.class, null);
        return CustomResponse.deleted(listMapper.fromEntityToData(list));
    }

    /**
     * Récupère la liste des articles d'une liste
     *
     * @param listid Identifiant de la liste
     * @param userid Identifiant de l'utilisateur
     * @return Liste des articles
     */
    public Response getListReferencesOfList(long listid, long userid) {
        List list = (List) ((ListDAO) getDAO()).getById(listid, userid);
        if (list == null) {
            throw new NotFoundException(List.class, listid);
        }

        java.util.List<ListReference> listReferences = ((ListReferenceDAO) getDAOByType(ListReference.class)).getListReferencesForList(listid);
        Mapper<ListReference> mapper = new Mapper<ListReference>(ListReference.class, null);
        return CustomResponse.found(mapper.fromEntitiesToDatas(listReferences));
    }

    /**
     * Met à jour la liste des articles dans la liste
     *
     * @param listid Identifiant de la liste
     * @param userid Identifiant de l'utilisateur
     * @param serialized Map contenant les éléments mis à jour sous forme de
     * chaîne au format JSON
     * @return Response avec le statut OK
     */
    public Response updateListReferencesOfList(long listid, long userid, String serialized) {
        Map<ListReferenceDTO, ? extends Object> mapUpdate = null;

        //On vérifie que la liste appartient bien à l'utilisateur
        List list = (List) ((ListDAO) getDAO()).getById(listid, userid);
        if (list == null) {
            throw new NotFoundException(List.class, listid);
        }

        try {
            //On désérialise la chaîne contenant la map des éléments à mettre à jour
            JsonObjectMapper<ListReferenceDTO> mapperListReference = new JsonObjectMapper<ListReferenceDTO>(ListReferenceDTO.class, EActionList.class);
            mapUpdate = mapperListReference.readMap(serialized);

            //Mapper pour transformer les DTO en Entity
            DozerMapper<ListReference, ListReferenceDTO> dozerMapper = new DozerMapper<ListReference, ListReferenceDTO>(ListReference.class, ListReferenceDTO.class);

            //On parcourt la la liste des clés de la map
            for (ListReferenceDTO listReferenceDTO : mapUpdate.keySet()) {
                EActionList action = (EActionList) mapUpdate.get(listReferenceDTO);
                ListReference listReferenceEntity = null;

                //Si il faut ajouter l'article, on transforme le DTO en une nouvelle entité
                if (action.equals(EActionList.ADD)) {
                    listReferenceEntity = dozerMapper.mapDTOToEntity(listReferenceDTO, ListReference.class);

                    //On charge les entités liées pour qu'elles soient à jour
                    listReferenceEntity.setList(list);
                    listReferenceEntity.setReference((Reference) getDAOByType(Reference.class).getById(Reference.class, listReferenceDTO.getId2()));

                    //On sauvegarde en base de données la nouvelle entité créée
                    getDAOByType(ListReference.class).persist(listReferenceEntity);
                } else {
                    //Sinon,  on récupère l'entité existante
                    listReferenceEntity = (ListReference) ((IAssociationDAO) getDAOByType(ListReference.class)).getByIds(listReferenceDTO.getId1(), listReferenceDTO.getId2());

                    //On transforme le DTO en l'entité existante
                    dozerMapper.mapDTOToEntity(listReferenceDTO, listReferenceEntity);

                    //On supprime l'entité
                    if (action.equals(EActionList.DELETE)) {
                        getDAOByType(ListReference.class).delete(listReferenceEntity);
                    } else {
                        //Sinon on la met à jour
                        getDAOByType(ListReference.class).merge(listReferenceEntity);
                    }
                }
            }

        } catch (SerializationException ex) {
            Logger.getLogger(Mapper.class.getName()).log(Level.SEVERE, null, ex);
            throw new MappingException(ex.getLocalizedMessage());
        }

        return CustomResponse.updated(null);
    }

}
