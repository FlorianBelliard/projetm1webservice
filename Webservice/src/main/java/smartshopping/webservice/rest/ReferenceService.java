/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import smartshopping.webservice.entity.Brand;
import smartshopping.webservice.entity.Reference;
import smartshopping.webservice.util.CustomResponse;

/**
 * Classe contenant les services liés aux références
 * @author FLORIAN
 */
@Path("/references")
public class ReferenceService extends GenericService<Reference> {

    /**
     * Service ajoutant une référence 
     * @param serialized Chaine contenant la référence à ajouter au format JSON
     * @param brandid Identifiant de la marque à laquelle la référence va être rattachée
     * @return Réponse HTTP contenant la référence ajoutée
     */
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response add(@FormParam("serialized") String serialized, @FormParam("brandid") long brandid) {
        Reference reference = (Reference) getMapper().fromDataToEntity(serialized);
        Brand brand = (Brand) getDAOByType(Brand.class).getById(Brand.class, brandid);

        reference.setBrand(brand);

        getDAO().persist(reference);
        return CustomResponse.created(getMapper().fromEntityToData(reference));
    }
    
    /***
     * Service mettant à jour une référence
     * @param serialized Chaine contenant la référence à mettre à jour au format JSON
     * @param brandid Identifiant de la marque à laquelle la référence est rattachée
     * @return Réponse HTTP contenant la référence mise à jour
     */
    @PUT
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response update(@FormParam("serialized") String serialized, @FormParam("brandid") long brandid) {
        Reference reference = (Reference) getMapper().fromDataToEntity(serialized);
        Brand brand = (Brand) getDAOByType(Brand.class).getById(Brand.class, brandid);

        reference.setBrand(brand);

        getDAO().merge(reference);
        return CustomResponse.created(getMapper().fromEntityToData(reference));
    }
}
