/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.rest;

import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import org.dozer.MappingException;
import smartshopping.webservice.dao.ItemDAO;
import smartshopping.webservice.dao.ListDAO;
import smartshopping.webservice.dao.ListReferenceDAO;
import smartshopping.webservice.dao.ShoppingItemDAO;
import smartshopping.webservice.dao.StoreDAO;
import smartshopping.webservice.dao.interfaces.IAssociationDAO;
import smartshopping.webservice.entity.Item;
import smartshopping.webservice.entity.List;
import smartshopping.webservice.entity.ListReference;
import smartshopping.webservice.entity.Shopping;
import smartshopping.webservice.entity.ShoppingItem;
import smartshopping.webservice.entity.Store;
import smartshopping.webservice.exceptions.NotFoundException;
import smartshopping.webservice.mapping.DozerMapper;
import smartshopping.webservice.mapping.Mapper;
import smartshopping.webservice.util.CustomResponse;
import smartshopping.webservicelib.accessservice.EActionList;
import smartshopping.webservicelib.exceptions.SerializationException;
import smartshopping.webservicelib.model.ShoppingItemDTO;
import smartshopping.webservicelib.serialization.JsonObjectMapper;

/**
 * Classe contenant les services liés aux achats dans une liste
 *
 * @author FLORIAN
 */
@Path("/shoppings")
public class ShoppingService extends GenericService<Shopping> {

    /**
     * Récupère la liste des articles d'une liste de courses
     *
     * @param listid Identifiant de la liste de courses
     * @param userid Identifiant de l'utilisateur
     * @return Liste des articles
     */
    public Response getShoppingItemsOfShopping(long listid, long userid) {
        List list = (List) ((ListDAO) getDAOByType(List.class)).getById(listid, userid);
        if (list == null) {
            throw new NotFoundException(List.class, listid);
        }

        java.util.List<ShoppingItem> shoppingItems = ((ShoppingItemDAO) getDAOByType(ShoppingItem.class)).getShoppingItemsForShopping(listid);
        Mapper<ShoppingItem> mapper = new Mapper<ShoppingItem>(ShoppingItem.class, null);
        return CustomResponse.found(mapper.fromEntitiesToDatas(shoppingItems));
    }

    /**
     * Créer une liste de courses à effectuer dans un magasin
     *
     * @param listid Identifiant de la liste
     * @param userid Identifiant de l'utilisateur
     * @param storeid Identifiant du magasin
     * @return Liste de courses créée
     */
    public Response doShopping(long listid, long userid, long storeid) {
        List list = (List) ((ListDAO) getDAOByType(List.class)).getById(listid, userid);
        if (list == null) {
            throw new NotFoundException(List.class, listid);
        }

        Store store = (Store) ((StoreDAO) getDAOByType(Store.class)).getById(Store.class, storeid);
        if (store == null) {
            throw new NotFoundException(Store.class, storeid);
        }

        Shopping shopping = new Shopping();
        shopping.setList(list);
        shopping.setStore(store);
        shopping.setPrice(0);
        shopping.setStatus(Boolean.FALSE);
        shopping.setDate(new Date());
        getDAO().persist(shopping);

        java.util.List<ListReference> listReferences = ((ListReferenceDAO) getDAOByType(ListReference.class)).getListReferencesForList(listid);

        for (ListReference listReference : listReferences) {
            Item item = ((ItemDAO) getDAOByType(Item.class)).getItemOfReferenceFromStore(listReference.getReference(), store);

            if (item != null) {
                ShoppingItem shoppingItem = new ShoppingItem(shopping, item, listReference.getQuantity(), Boolean.FALSE);
                getDAOByType(ShoppingItem.class).persist(shoppingItem);
            } else {
                //TODO : traiter le cas où le produit n'est pas disponible dans le magasin choisi
            }
        }

        Mapper<Shopping> mapper = new Mapper<Shopping>(Shopping.class, null);

        return CustomResponse.created(mapper.fromEntityToData(shopping));
    }

    /**
     * Supprime une liste de courses à effectuer dans un magasin
     *
     * @param listid Identifiant de la liste
     * @param userid Identifiant de l'utilisateur
     * @return Liste de courses à effectuer supprimée
     */
    public Response deleteShopping(long listid, long userid) {
        List list = (List) ((ListDAO) getDAOByType(List.class)).getById(listid, userid);
        if (list == null) {
            throw new NotFoundException(List.class, listid);
        }

        Shopping shopping = list.getShopping();
        getDAO().delete(shopping);

        return CustomResponse.deleted(shopping);
    }

    /**
     * Met à jour la liste des articles dans la liste
     *
     * @param listid Identifiant de la liste
     * @param userid Identifiant de l'utilisateur
     * @param serialized Map contenant les éléments mis à jour sous forme de
     * chaîne au format JSON
     * @return Response avec le statut OK
     */
    public Response updateShoppingItemsOfShopping(long listid, long userid, String serialized) {
        Map<ShoppingItemDTO, ? extends Object> mapUpdate = null;

        //On vérifie que la liste appartient bien à l'utilisateur
        List list = (List) ((ListDAO) getDAOByType(List.class)).getById(listid, userid);
        if (list == null) {
            throw new NotFoundException(List.class, listid);
        }

        try {
            //On désérialise la chaîne contenant la map des éléments à mettre à jour
            JsonObjectMapper<ShoppingItemDTO> mapperShoppingItem = new JsonObjectMapper<ShoppingItemDTO>(ShoppingItemDTO.class, EActionList.class);
            mapUpdate = mapperShoppingItem.readMap(serialized);

            //Mapper pour transformer les DTO en Entity
            DozerMapper<ShoppingItem, ShoppingItemDTO> dozerMapper = new DozerMapper<ShoppingItem, ShoppingItemDTO>(ShoppingItem.class, ShoppingItemDTO.class);

            //On parcourt la la liste des clés de la map
            for (ShoppingItemDTO shoppingItemDTO : mapUpdate.keySet()) {
                EActionList action = (EActionList) mapUpdate.get(shoppingItemDTO);
                ShoppingItem shoppingItemEntity = null;

                //Si il faut ajouter l'article, on transforme le DTO en une nouvelle entité
                if (action.equals(EActionList.ADD)) {
                    shoppingItemEntity = dozerMapper.mapDTOToEntity(shoppingItemDTO, ShoppingItem.class);

                    //On charge les entités liées pour qu'elles soient à jour
                    shoppingItemEntity.setShopping((Shopping) getDAOByType(Shopping.class).getById(Shopping.class, listid));
                    shoppingItemEntity.setItem((Item) getDAOByType(Item.class).getById(Item.class, shoppingItemDTO.getId2()));

                    //On sauvegarde en base de données la nouvelle entité créée
                    getDAOByType(ShoppingItem.class).persist(shoppingItemEntity);
                } else {
                    //Sinon,  on récupère l'entité existante
                    shoppingItemEntity = (ShoppingItem) ((IAssociationDAO) getDAOByType(ShoppingItem.class)).getByIds(shoppingItemDTO.getId1(), shoppingItemDTO.getId2());

                    //On transforme le DTO en l'entité existante
                    dozerMapper.mapDTOToEntity(shoppingItemDTO, shoppingItemEntity);

                    //On supprime l'entité
                    if (action.equals(EActionList.DELETE)) {
                        getDAOByType(ShoppingItem.class).delete(shoppingItemEntity);
                    } else {
                        //Sinon on la met à jour
                        getDAOByType(ShoppingItem.class).merge(shoppingItemEntity);
                    }
                }
            }

        } catch (SerializationException ex) {
            Logger.getLogger(Mapper.class.getName()).log(Level.SEVERE, null, ex);
            throw new MappingException(ex.getLocalizedMessage());
        }

        return CustomResponse.updated(null);
    }
}
