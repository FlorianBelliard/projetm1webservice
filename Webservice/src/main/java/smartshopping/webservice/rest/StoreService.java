/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.rest;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import smartshopping.webservice.dao.ItemDAO;
import smartshopping.webservice.dao.StoreDAO;
import smartshopping.webservice.dao.UserDAO;
import smartshopping.webservice.entity.Chain;
import smartshopping.webservice.entity.Item;
import smartshopping.webservice.entity.Store;
import smartshopping.webservice.entity.User;
import smartshopping.webservice.mapping.Mapper;
import smartshopping.webservice.util.CustomResponse;

/**
 * Classe contenant les services liés aux magasins
 *
 * @author FLORIAN
 */
@Path("/stores")
public class StoreService extends GenericService<Store> {

    /**
     * Service retournant tous les produits d'un magasin
     *
     * @param id Identifiant d'un magasin
     * @return Réponse HTTP contenant la liste des produits
     */
    @GET
    @Path("/{id}/items/")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response getItemsOfStore(@PathParam("id") long id) {
        List<Item> items = ((ItemDAO) getDAOByType(Item.class)).getItemsForStore(id);
        Mapper<Item> mapper = new Mapper<Item>(Item.class, null);
        return CustomResponse.found(mapper.fromEntitiesToDatas(items));
    }

    /**
     * Service ajoutant un magasin
     *
     * @param serialized Chaine contenant le magasin à ajouter au format JSON
     * @param chainid Identifiant de la chaîne à laquelle le magasin va être
     * rattaché
     * @return Réponse HTTP contenant le magasin créé
     */
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response add(@FormParam("serialized") String serialized, @FormParam("chainid") long chainid) {
        Store store = (Store) getMapper().fromDataToEntity(serialized);
        Chain chain = (Chain) getDAOByType(Chain.class).getById(Chain.class, chainid);

        store.setChain(chain);

        getDAO().persist(store);
        return CustomResponse.created(getMapper().fromEntityToData(store));
    }

    /**
     * Service mettant à jour un magasin
     *
     * @param serialized Chaine contenant le magasin à mettre à jour au format
     * JSON
     * @param chainid Identifiant de la chaine à laquelle le magasin est
     * rattaché
     * @return Réponse HTTP contenant le magasin mis à jour
     */
    @PUT
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response update(@FormParam("serialized") String serialized, @FormParam("chainid") long chainid) {
        Store store = (Store) getMapper().fromDataToEntity(serialized);
        Chain chain = (Chain) getDAOByType(Chain.class).getById(Chain.class, chainid);

        store.setChain(chain);

        getDAO().merge(store);
        return CustomResponse.updated(getMapper().fromEntityToData(store));
    }

    /**
     * Service récupérant les magasins favoris d'un utilisateur
     *
     * @param userid Identifiant de l'utilisateur
     * @return Liste des magasins favoris
     */
    public Response getFavoriteStoresOfUser(long userid) {
        List<Store> favoritesStores = ((StoreDAO) getDAO()).getStoresForUser(userid);
        return CustomResponse.found(getMapper().fromEntitiesToDatas(favoritesStores));
    }

    /**
     * Service ajoutant un magasin aux favoris d'un utilisateur
     *
     * @param userid Identifiant de l'utilisateur
     * @param storeid Identifiant du magasin
     * @return Magasin ajouté aux favoris
     */
    public Response addFavoriteStoreToUser(long userid, long storeid) {
        User user = ((UserDAO) getDAOByType(User.class)).getById(User.class, userid);
        Store store = ((StoreDAO) getDAO()).getById(Store.class, storeid);

        List<User> users = ((UserDAO) getDAOByType(User.class)).getUsersForStore(storeid);

        if (users != null) {
            store.setUsers(new HashSet<User>(users));
        }

        store.getUsers().add(user);
        getDAO().merge(store);

        return CustomResponse.created(getMapper().fromEntityToData(store));
    }

    /**
     * Service supprimant un magasin des favoris d'un utilisateur
     *
     * @param userid Identifiant de l'utilisateur
     * @param storeid Identifiant du magasin
     * @return Magasin supprimé des favoris
     */
    public Response removeFavoriteStoreFromUser(long userid, long storeid) {
        User user = ((UserDAO) getDAOByType(User.class)).getById(User.class, userid);

        Set<String> storePropertiesToFetch = new HashSet<String>();
        storePropertiesToFetch.add("users");
        Store store = ((StoreDAO) getDAO()).getById(Store.class, storeid, storePropertiesToFetch);

        store.getUsers().remove(user);

        getDAO().merge(store);

        return CustomResponse.deleted(getMapper().fromEntityToData(store));
    }
}
