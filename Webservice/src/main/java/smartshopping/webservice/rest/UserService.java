/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import smartshopping.webservice.dao.ListReferenceDAO;
import smartshopping.webservice.dao.ShoppingItemDAO;
import smartshopping.webservice.dao.UserDAO;
import smartshopping.webservice.entity.ListReference;
import smartshopping.webservice.entity.ShoppingItem;
import smartshopping.webservice.entity.User;
import smartshopping.webservice.mapping.Mapper;
import smartshopping.webservice.util.CustomResponse;

/**
 * Classe contenant les services liés aux utilisateurs
 *
 * @author FLORIAN
 */
@Path("/users")
public class UserService extends GenericService<User> {

    /**
     * Service indiquant si l'utilisateur existe
     *
     * @param login Login de l'utilisateur
     * @param password Mot de passe de l'utilisateur
     * @return Réponse HTTP contenant l'utilisateur trouvé
     */
    @POST
    @Path("/exists")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response isUserExist(@FormParam("login") String login, @FormParam("password") String password) {
        User user = ((UserDAO) getDAO()).isUserExist(login, password);
        String userSerialized = null;

        if (user != null) {
            userSerialized = getMapper().fromEntityToData(user);
        } else {
            userSerialized = null;
        }

        return CustomResponse.found(userSerialized);
    }

    /**
     * Service retournant la liste des magasins favoris d'un utilisateur
     *
     * @param userid Identifiant de l'utilisateur
     * @return Réponse HTTP contenant la liste des magasins favoris
     */
    @GET
    @Path("/{userid}/favoriteStores")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response getFavoritesStoresOfUser(@PathParam("userid") long userid) {
        StoreService storeService = new StoreService();
        return storeService.getFavoriteStoresOfUser(userid);
    }

    /**
     * Service ajoutant un magasin aux favoris d'un utilisateur
     *
     * @param userid Identifiant de l'utilisateur
     * @param storeid Identifiant du magasin
     * @return Réponse HTTP contenant le magasin ajouté aux favoris
     */
    @POST
    @Path("/{userid}/favoriteStores")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response addFavoriteStoreToUser(@PathParam("userid") long userid, @FormParam("storeid") long storeid) {
        StoreService storeService = new StoreService();
        return storeService.addFavoriteStoreToUser(userid, storeid);
    }

    /**
     * Service supprimant un magasin des favoris d'un utilisateur
     *
     * @param userid Identifiant de l'utilisateur
     * @param storeid Identifiant de l'utilisateur
     * @return Réponse HTTP contenant le magasin supprimé des favoris
     */
    @DELETE
    @Path("/{userid}/favoriteStores/{storeid}")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response removeFavoriteStoreFromUser(@PathParam("userid") long userid, @PathParam("storeid") long storeid) {
        StoreService storeService = new StoreService();
        return storeService.removeFavoriteStoreFromUser(userid, storeid);
    }

    /**
     * Service retournant toutes les listes d'un utilisateur
     *
     * @param userid Identifiant de l'utilisateur
     * @return Réponse HTTP contenant la liste de listes d'un utilisateur
     */
    @GET
    @Path("/{userid}/lists")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response getAllListsOfUser(@PathParam("userid") long userid) {
        ListService listService = new ListService();
        return listService.getAllListsOfUser(userid);
    }

    /**
     * Service retournant une liste d'un utilisateur
     *
     * @param listid Identifiant de la liste
     * @param userid Identifiant de l'utilisateur
     * @return Réponse HTTP contenant la liste
     */
    @GET
    @Path("/{userid}/lists/{listid}")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response getListOfUser(@PathParam("listid") long listid, @PathParam("userid") long userid) {
        ListService listService = new ListService();
        return listService.getListOfUser(listid, userid);
    }

    /**
     * Service ajoutant une liste à un utilisateur
     *
     * @param userid Identifiant de l'utilisateur
     * @param serialized Chaîne contenant la liste à ajouter au format JSON
     * @return Réponse HTTP contenant la liste ajoutée
     */
    @POST
    @Path("/{userid}/lists")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response addListToUser(@PathParam("userid") long userid, String serialized) {
        ListService listService = new ListService();
        return listService.addListToUser(userid, serialized);
    }

    /**
     * Service surpprimant une liste d'un utilisateur
     *
     * @param listid Identifiant de la liste
     * @param userid Identifiant de l'utilisateur
     * @return Réponse HTTP contenant la liste supprimée
     */
    @DELETE
    @Path("/{userid}/lists/{listid}")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response removeListFromUser(@PathParam("listid") long listid, @PathParam("userid") long userid) {
        ListService listService = new ListService();
        return listService.removeListFromUser(listid, userid);
    }

    /**
     * Service retournant la liste des articles d'une liste
     *
     * @param listid Identifiant de la liste
     * @param userid Identifiant de l'utilisateur
     * @return Réponse HTTP contenant la liste des articles
     */
    @GET
    @Path("/{userid}/lists/{listid}/listReferences")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response getListReferencesOfList(@PathParam("listid") long listid, @PathParam("userid") long userid) {
        ListService listService = new ListService();
        return listService.getListReferencesOfList(listid, userid);
    }

    /**
     * Service retournant une référence dans une liste
     *
     * @param idList Identifiant de la liste
     * @param idReference Identifiant d'une référence
     * @return Référence dans une liste
     */
    @GET
    @Path("/{userid}/lists/{listid}/listReferences/{referenceid}")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response getListReferenceByIds(@PathParam("listid") long listid, @PathParam("referenceid") long referenceid) {
        ListReference listReference = (ListReference) ((ListReferenceDAO) getDAOByType(ListReference.class)).getByIds(listid, referenceid);
        Mapper<ListReference> listReferenceMapper = new Mapper<ListReference>(ListReference.class, null);
        return CustomResponse.found(listReferenceMapper.fromEntityToData(listReference));
    }

    /**
     * Service mettant à jour la liste des articles d'une liste
     *
     * @param listid Identifiant de la liste
     * @param userid Identfiant de l'utilisateur
     * @param serialized Chaine contenant une map des articles modifiés au
     * format JSON
     * @return Réponse HTTP indiquant le résultat de la mise à jour
     */
    @PUT
    @Path("/{userid}/lists/{listid}/listReferences")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response updateListReferencesOfList(@PathParam("listid") long listid, @PathParam("userid") long userid, String serialized) {
        ListService listService = new ListService();
        return listService.updateListReferencesOfList(listid, userid, serialized);
    }

    /**
     * Service créant une liste de courses à effectuer dans un magasin
     *
     * @param listid Identifiant de la liste
     * @param userid Identifiant de l'utilisateur
     * @param storeid Identifiant du magasin
     * @return Réponse HTTP contenant la liste de courses à effectuer créée
     */
    @POST
    @Path("/{userid}/lists/{listid}/shopping")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response doShoppingForList(@PathParam("listid") long listid, @PathParam("userid") long userid, @QueryParam("storeid") long storeid) {
        ShoppingService shoppingService = new ShoppingService();
        return shoppingService.doShopping(listid, userid, storeid);
    }

    /**
     * Service supprimant une liste de courses à effectuer dans un magasin
     *
     * @param listid Identifiant de la liste
     * @param userid Identifiant de l'utilisateur
     * @return Réponse HTTP contenant la liste de courses à effectuer supprimée
     */
    @DELETE
    @Path("/{userid}/lists/{listid}/shopping")
    public Response deleteShoppingForList(@PathParam("listid") long listid, @PathParam("userid") long userid) {
        ShoppingService shoppingService = new ShoppingService();
        return shoppingService.deleteShopping(listid, userid);
    }

    /**
     * Service retournant la liste des articles dans une liste de courses
     *
     * @param listid Identifiant de la liste
     * @param userid Identifiant de l'utilisateur
     * @return Liste des articles dans la liste de courses à effectuer
     */
    @GET
    @Path("/{userid}/lists/{listid}/shopping/shoppingItems")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response getShoppingItemsForShopping(@PathParam("listid") long listid, @PathParam("userid") long userid) {
        ShoppingService shoppingService = new ShoppingService();
        return shoppingService.getShoppingItemsOfShopping(listid, userid);
    }

    /**
     * Service retournant un article dans une liste de courses à effectuer
     *
     * @param idShopping Idenfifiant de la liste de courses à effectuer
     * @param idItem Identifiant de l'article
     * @return Article dans la liste de courses à effectuer
     */
    @GET
    @Path("/{userid}/lists/{listid}/shopping/shoppingItems/{itemid}")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response getShoppingItemByIds(@PathParam("listid") long shoppingid, @PathParam("itemid") long itemid) {
        ShoppingItem shoppingItem = (ShoppingItem) ((ShoppingItemDAO) getDAOByType(ShoppingItemDAO.class)).getByIds(shoppingid, itemid);
        Mapper<ShoppingItem> mapperShoppingItem = new Mapper<ShoppingItem>(ShoppingItem.class, null);
        return CustomResponse.found(mapperShoppingItem.fromEntityToData(shoppingItem));
    }

    /**
     * Service mettant à jour la liste des articles d'une liste
     *
     * @param listid Identifiant de la liste
     * @param userid Identfiant de l'utilisateur
     * @param serialized Chaine contenant une map des articles modifiés au
     * format JSON
     * @return Réponse HTTP indiquant le résultat de la mise à jour
     */
    @PUT
    @Path("/{userid}/lists/{listid}/shopping/shoppingItems")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response updateShoppingItemsOfShopping(@PathParam("listid") long listid, @PathParam("userid") long userid, String serialized) {
        ShoppingService shoppingService = new ShoppingService();
        return shoppingService.updateShoppingItemsOfShopping(listid, userid, serialized);
    }
}
