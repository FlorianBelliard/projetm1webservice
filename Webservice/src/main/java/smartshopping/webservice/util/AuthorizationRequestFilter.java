/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.util;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import smartshopping.webservice.dao.AdminDAO;
import smartshopping.webservice.dao.UserDAO;
import smartshopping.webservice.entity.Admin;
import smartshopping.webservice.entity.Person;
import smartshopping.webservice.entity.User;
import smartshopping.webservicelib.security.BasicAuthentification;

/**
 * Filtre qui permet de gérer les accès aux différents services
 * @author FLORIAN
 */
@Provider
public class AuthorizationRequestFilter implements ContainerRequestFilter {

    private static final Set<String> authorizedUri = new HashSet<String>();

    static {
        authorizedUri.add("/stores");
        authorizedUri.add("/brands");
        authorizedUri.add("/items");
        authorizedUri.add("/references");
        authorizedUri.add("/chains");
    }

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
//        String uri = requestContext.getUriInfo().getPath(true);
//
//        if (requestContext.getHeaderString("Authorization") == null) {
//            if ((!uri.equals("/users")) && (!requestContext.getMethod().equals("POST")) && (!uri.equals("/users/exists")) && (!uri.equals("/admins/exists"))) {
//                throw new WebApplicationException(Response.Status.UNAUTHORIZED);
//            }
//        } else {
//            String[] authentification = BasicAuthentification.decode(requestContext.getHeaderString("Authorization"));
//            if (authentification == null) {
//                throw new WebApplicationException(Response.Status.UNAUTHORIZED);
//            }
//
//            UserDAO userDAO = new UserDAO();
//            Person person = userDAO.isUserExist(authentification[0], authentification[1]);
//
//            if (person == null) {
//                AdminDAO adminDAO = new AdminDAO();
//                person = adminDAO.isAdminExist(authentification[0], authentification[1]);
//            }
//
//            if (person == null) {
//                throw new WebApplicationException(Response.Status.UNAUTHORIZED);
//            }
//
//            String uriCollection = uri.substring(0, uri.indexOf("/", 1));
//
//            if (person instanceof Admin) {
//                if (!authorizedUri.contains(uriCollection)) {
//                    throw new WebApplicationException(Response.Status.UNAUTHORIZED);
//                }
//            } else if (person instanceof User) {
//                if (uriCollection.equals("/users")) {
//                    if (!uri.startsWith("/users/" + person.getId())) {
//                        throw new WebApplicationException(Response.Status.UNAUTHORIZED);
//                    }
//                } else if ((!authorizedUri.contains(uriCollection)) || ((authorizedUri.contains(uriCollection)) && (!requestContext.getMethod().equals("GET")))) {
//                    throw new WebApplicationException(Response.Status.UNAUTHORIZED);
//                }
//            }
//        }
    }
}
