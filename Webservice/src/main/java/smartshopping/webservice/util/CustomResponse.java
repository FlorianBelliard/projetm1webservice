/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservice.util;

import javax.ws.rs.core.Response;

/**
 *
 * @author FLORIAN
 */
public class CustomResponse {

    public static Response created(Object o) {
        return Response.status(Response.Status.CREATED).entity(o).build();
    }

    public static Response updated(Object o) {
        return Response.status(Response.Status.OK).entity(o).build();
    }

    public static Response deleted(Object o) {
        return Response.status(Response.Status.OK).entity(o).build();
    }

    public static Response found(Object o) {
        return Response.status(Response.Status.OK).entity(o).build();
    }
}
