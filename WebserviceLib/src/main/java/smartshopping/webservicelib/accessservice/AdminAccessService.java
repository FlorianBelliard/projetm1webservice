/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservicelib.accessservice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import smartshopping.webservicelib.exceptions.SerializationException;
import smartshopping.webservicelib.exceptions.ServiceException;
import smartshopping.webservicelib.exceptions.ServiceNotAvailable;
import smartshopping.webservicelib.model.AdminDTO;
import smartshopping.webservicelib.util.WebserviceContext;

/**
 * Classe facilitant l'accès aux services liés aux administrateurs
 * @author FLORIAN
 */
public class AdminAccessService extends GenericAccessService<AdminDTO> {

    /**
     * Constructeur
     * @param webServiceContext contient les informations nécessaires pour accéder aux services
     */
    public AdminAccessService(WebserviceContext webServiceContext) {
        super(webServiceContext);
    }

    /**
     * Indique si un administrateur existe
     * @param login Login de l'administrateur
     * @param password Mot de passe de l'administrateur
     * @return Administrateur trouvé
     * @throws SerializationException
     * @throws ServiceNotAvailable
     * @throws ServiceException 
     */
    public AdminDTO isAdminExists(String login, String password) throws SerializationException, ServiceNotAvailable, ServiceException {
        AdminDTO admin = null;
        String url = null;
        try {
            url = webServiceContext.getUrl() + getWebserviceEntityCollectionName() + "/exists";
            HttpPost request = new HttpPost(url);
            List<NameValuePair> values = new ArrayList<NameValuePair>(2);
            values.add(new BasicNameValuePair("login", login));
            values.add(new BasicNameValuePair("password", password));
            UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(values, "UTF-8");
            request.setHeader("Content-Type", "application/x-www-form-urlencoded");
            request.setEntity(formEntity);
            HttpResponse response = client.execute(request);
            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }

            if (responseContent.isEmpty()) {
                admin = null;
            } else {
                admin = (AdminDTO) jsonMapper.read(responseContent);
            }
        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }

        return admin;
    }

}
