/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package smartshopping.webservicelib.accessservice;

import smartshopping.webservicelib.model.ChainDTO;
import smartshopping.webservicelib.util.WebserviceContext;

/**
 * Classe facilitant l'accès aux services liés aux chaînes
 * @author Alexis
 */
public class ChainAccessService extends GenericAccessService<ChainDTO> {
    
    /**
     * Constructeur
     * @param webServiceContext contient les informations nécessaires pour accéder aux services
     */
    public ChainAccessService(WebserviceContext webServiceContext) {
        super(webServiceContext);
    }
    
}
