/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package smartshopping.webservicelib.accessservice;

/**
 * Enumération indiquant les actions à effectuer dans le cas d'une mise à jour dans une liste
 * @author fbelliar
 */
public enum EActionList {
    
    ADD,
    DELETE,
    UPDATE
}
