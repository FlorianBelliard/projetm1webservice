/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservicelib.accessservice;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;
import smartshopping.webservicelib.exceptions.SerializationException;
import smartshopping.webservicelib.exceptions.ServiceException;
import smartshopping.webservicelib.exceptions.ServiceNotAvailable;
import smartshopping.webservicelib.model.DTO;
import smartshopping.webservicelib.security.BasicAuthentification;
import smartshopping.webservicelib.security.SSLDefaultHttpClient;
import smartshopping.webservicelib.serialization.JsonObjectMapper;
import smartshopping.webservicelib.util.WebserviceContext;

/**
 * Classe générique pour accéder au webservice
 *
 * @author FLORIAN
 * @param <T>
 */
public abstract class GenericAccessService<T extends DTO> {

    /**
     * Permet la sérialisation/désérialisation des DTO
     */
    protected JsonObjectMapper jsonMapper;

    /**
     * Type du DTO
     */
    private final Class<T> type;

    /**
     * Client HTTP
     */
    protected HttpClient client;

    /**
     * Contient les informations nécessaires à l'accès aux services
     */
    protected WebserviceContext webServiceContext;

    /**
     * Constructeur
     *
     * @param webServiceContext contient les informations nécessaires pour
     * accéder aux services
     */
    public GenericAccessService(WebserviceContext webServiceContext) {
        this.type = ((Class) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0]);
        this.jsonMapper = new JsonObjectMapper(type);

        //Utilisation d'un SSLDefaultHttpClient pour avoir une connexion sécurisée en SSL
        this.client = new SSLDefaultHttpClient();
        //Utilisation d'un DefaultHttClient pour une connexion non sécurisée
        //(peut servir pendant le développement pour éviter de mettre en place les certficats
//        this.client = new DefaultHttpClient();

        this.webServiceContext = webServiceContext;

        //Permet de tester via Fiddler par exemple que le flux est bien en HTTPS
        //HttpHost proxy = new HttpHost("127.0.0.1", 8888, "http");
        //client.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
    }

    /**
     * Récupère un DTO
     *
     * @param id Identifiant du DTO
     * @return DTO
     * @throws SerializationException
     * @throws ServiceNotAvailable
     * @throws ServiceException
     */
    public T getById(long id) throws SerializationException, ServiceNotAvailable, ServiceException {
        T dto = null;
        String url = null;
        try {
            url = webServiceContext.getUrl() + getWebserviceEntityCollectionName() + "/" + id;
            HttpGet request = new HttpGet(url);
            initializeAuthentification(request);
            HttpResponse response = client.execute(request);
            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }

            dto = (T) jsonMapper.read(responseContent);
        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }

        return dto;
    }

    /**
     * Récupère tous les DTOs
     *
     * @return Liste de DTO
     * @throws SerializationException
     * @throws ServiceException
     * @throws ServiceNotAvailable
     */
    public List<T> getAll() throws SerializationException, ServiceException, ServiceNotAvailable {
        List<T> dtos = null;
        String url = null;

        try {
            url = webServiceContext.getUrl() + getWebserviceEntityCollectionName() + "/";
            HttpGet request = new HttpGet(url);
            initializeAuthentification(request);
            HttpResponse response = client.execute(request);
            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }

            dtos = jsonMapper.readList(responseContent);
        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }

        return dtos;
    }

    /**
     * Ajoute un DTO
     *
     * @param dto DTO
     * @return DTO ajouté
     * @throws ServiceException
     * @throws ServiceNotAvailable
     * @throws SerializationException
     */
    public T add(T dto) throws ServiceException, ServiceNotAvailable, SerializationException {
        String url = null;
        try {
            url = webServiceContext.getUrl() + getWebserviceEntityCollectionName() + "/";
            HttpPost request = new HttpPost(url);
            initializeAuthentification(request);
            request.setEntity(new StringEntity(jsonMapper.write(dto)));
            HttpResponse response = client.execute(request);
            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 201) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }

            dto = (T) jsonMapper.read(responseContent);
        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }

        return dto;
    }

    /**
     * Supprime un DTO
     *
     * @param id Identifiant du DTO
     * @return DTO supprimé
     * @throws ServiceException
     * @throws ServiceNotAvailable
     * @throws SerializationException
     */
    public T delete(long id) throws ServiceException, ServiceNotAvailable, SerializationException {
        String url = null;
        T dto = null;
        try {
            url = webServiceContext.getUrl() + getWebserviceEntityCollectionName() + "/" + id;
            HttpDelete request = new HttpDelete(url);
            initializeAuthentification(request);
            HttpResponse response = client.execute(request);
            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }

            dto = (T) jsonMapper.read(responseContent);
        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }

        return dto;
    }

    /**
     * Met à jour un DTO
     *
     * @param dto DTO à mettre à jour
     * @return DTo mis à jour
     * @throws SerializationException
     * @throws ServiceException
     * @throws ServiceNotAvailable
     */
    public T update(T dto) throws SerializationException, ServiceException, ServiceNotAvailable {
        String url = null;

        try {
            url = webServiceContext.getUrl() + getWebserviceEntityCollectionName() + "/";
            HttpPut request = new HttpPut(url);
            initializeAuthentification(request);
            request.setEntity(new StringEntity(jsonMapper.write(dto)));
            HttpResponse response = client.execute(request);
            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }

            dto = (T) jsonMapper.read(responseContent);
        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }

        return dto;
    }

    /**
     * Cherche dans les DTOS
     *
     * @param value Valeur recherchée
     * @param filters Filtres à appliquer à la recherche
     * @return Liste des DTOs trouvés
     * @throws SerializationException
     * @throws ServiceException
     * @throws ServiceNotAvailable
     */
    public List<T> search(String value, List<String> filters) throws SerializationException, ServiceException, ServiceNotAvailable {
        List<T> dtos = null;
        String url = null;

        try {
            List<NameValuePair> values = new ArrayList<NameValuePair>();
            values.add(new BasicNameValuePair("value", value));

            for (String filter : filters) {
                values.add(new BasicNameValuePair("filters", filter));
            }

            url = webServiceContext.getUrl() + getWebserviceEntityCollectionName() + "/search?" + URLEncodedUtils.format(values, "UTF-8");

            HttpGet request = new HttpGet(url);
            initializeAuthentification(request);
            HttpResponse response = client.execute(request);
            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }

            dtos = jsonMapper.readList(responseContent);
        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }

        return dtos;
    }

    /**
     * Permet d'obtenir le nom de la collection d'entités à requêter sur le
     * webservice Exemple "brands" pour la classe BrandDTO
     *
     * @return Nom de la collection d'entités
     */
    protected String getWebserviceEntityCollectionName() {
        return this.type.getSimpleName().substring(0, this.type.getSimpleName().length() - 3).toLowerCase() + "s";
    }

    /**
     * Convertit l'InputStream récupérée dans la HttpResponse en String
     *
     * @param is InputStream
     * @return String
     */
    protected String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is, "UTF-8").useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    /**
     * Permet d'ajouter une authentification à une requête
     *
     * @param request Requête
     */
    protected void initializeAuthentification(HttpRequest request) {
        if (webServiceContext.getPerson() != null) {
            request.addHeader("Authorization", "Basic " + BasicAuthentification.encode(webServiceContext.getPerson()));
        }
    }
}
