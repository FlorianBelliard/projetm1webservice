/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservicelib.accessservice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import smartshopping.webservicelib.exceptions.SerializationException;
import smartshopping.webservicelib.exceptions.ServiceException;
import smartshopping.webservicelib.exceptions.ServiceNotAvailable;
import smartshopping.webservicelib.model.ItemDTO;
import smartshopping.webservicelib.util.WebserviceContext;

/**
 * Classe facilitant l'accès aux services liés aux articles
 * @author FLORIAN
 */
public class ItemAccessService extends GenericAccessService<ItemDTO> {

    /**
     * Constructeur
     * @param webServiceContext contient les informations nécessaires pour accéder aux services
     */
    public ItemAccessService(WebserviceContext webServiceContext) {
        super(webServiceContext);
    }

    /**
     * Ajoute un article 
     * @param item Article à ajouter
     * @param referenceid Identifiant de la référence de l'article
     * @param storeid Identifiant du magasin de l'article
     * @return Article ajouté
     * @throws ServiceException
     * @throws ServiceNotAvailable
     * @throws SerializationException 
     */
    public ItemDTO add(ItemDTO item, long referenceid, long storeid) throws ServiceException, ServiceNotAvailable, SerializationException {
        String url = null;
        try {
            url = webServiceContext.getUrl() + getWebserviceEntityCollectionName() + "/";
            HttpPost request = new HttpPost(url);
            initializeAuthentification(request);
            List<NameValuePair> values = new ArrayList<NameValuePair>(3);
            values.add(new BasicNameValuePair("serialized", jsonMapper.write(item)));
            values.add(new BasicNameValuePair("referenceid", String.valueOf(referenceid)));
            values.add(new BasicNameValuePair("storeid", String.valueOf(storeid)));
            UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(values, "UTF-8");
            request.setHeader("Content-Type", "application/x-www-form-urlencoded");
            request.setEntity(formEntity);
            HttpResponse response = client.execute(request);
            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 201) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }

            item = (ItemDTO) jsonMapper.read(responseContent);
        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }

        return item;
    }

    /**
     * Cherche les articles dans le magasin
     * @param value Valeur recherchée
     * @param storeId Identifiant du magasin
     * @return Liste des articles trouvés
     * @throws SerializationException
     * @throws ServiceException
     * @throws ServiceNotAvailable 
     */
    public List<ItemDTO> searchByStore(String value, long storeId) throws SerializationException, ServiceException, ServiceNotAvailable {
        List<String> filters = new ArrayList<String>();
        filters.add(String.valueOf(storeId));
        return search(value, filters);
    }

}
