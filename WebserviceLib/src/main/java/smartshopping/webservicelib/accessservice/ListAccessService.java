/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservicelib.accessservice;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import smartshopping.webservicelib.exceptions.SerializationException;
import smartshopping.webservicelib.exceptions.ServiceException;
import smartshopping.webservicelib.exceptions.ServiceNotAvailable;
import smartshopping.webservicelib.model.ListDTO;
import smartshopping.webservicelib.model.ListReferenceDTO;
import smartshopping.webservicelib.serialization.JsonObjectMapper;
import smartshopping.webservicelib.util.WebserviceContext;

/**
 * Classe facilitant l'accès aux services liés aux listes
 *
 * @author FLORIAN
 */
public class ListAccessService extends GenericAccessService<ListDTO> {

    /**
     * Constructeur
     *
     * @param webServiceContext contient les informations nécessaires pour
     * accéder aux services
     */
    public ListAccessService(WebserviceContext webServiceContext) {
        super(webServiceContext);
    }

    /**
     * Récupère toutes les listes de l'utilisateur
     *
     * @return Liste des listes
     * @throws ServiceException
     * @throws SerializationException
     * @throws ServiceNotAvailable
     */
    public List<ListDTO> getAllListsOfUser() throws ServiceException, SerializationException, ServiceNotAvailable {
        String url = null;
        List<ListDTO> lists = null;
        try {
            url = webServiceContext.getUrl() + "users/" + webServiceContext.getPerson().getId() + "/" + getWebserviceEntityCollectionName();
            HttpGet request = new HttpGet(url);
            initializeAuthentification(request);
            HttpResponse response = client.execute(request);
            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }

            JsonObjectMapper<ListDTO> listMapper = new JsonObjectMapper<ListDTO>(ListDTO.class);
            lists = listMapper.readList(responseContent);
        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }
        return lists;
    }

    /**
     * Récupère une liste de l'utilisateur
     *
     * @param listid Identifiant de la liste
     * @return Liste
     * @throws ServiceException
     * @throws SerializationException
     * @throws ServiceNotAvailable
     */
    public ListDTO getListOfUser(long listid) throws ServiceException, SerializationException, ServiceNotAvailable {
        String url = null;
        ListDTO list = null;
        try {
            url = webServiceContext.getUrl() + "users/" + webServiceContext.getPerson().getId() + "/" + getWebserviceEntityCollectionName() + "/" + listid;
            HttpGet request = new HttpGet(url);
            initializeAuthentification(request);
            HttpResponse response = client.execute(request);
            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }

            JsonObjectMapper<ListDTO> listMapper = new JsonObjectMapper<ListDTO>(ListDTO.class);
            list = listMapper.read(responseContent);
        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }
        return list;
    }

    /**
     * Ajoute une liste à l'utilisateur
     *
     * @param list Liste à ajouter
     * @return Liste ajoutée
     * @throws SerializationException
     * @throws ServiceException
     * @throws ServiceNotAvailable
     */
    public ListDTO addListToUser(ListDTO list) throws SerializationException, ServiceException, ServiceNotAvailable {
        String url = null;
        try {
            url = webServiceContext.getUrl() + "users/" + webServiceContext.getPerson().getId() + "/" + getWebserviceEntityCollectionName();
            HttpPost request = new HttpPost(url);
            initializeAuthentification(request);
            JsonObjectMapper<ListDTO> listMapper = new JsonObjectMapper<ListDTO>(ListDTO.class);
            request.setEntity(new StringEntity(listMapper.write(list)));
            HttpResponse response = client.execute(request);
            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 201) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }

            list = (ListDTO) listMapper.read(responseContent);
        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }

        return list;
    }

    /**
     * Supprime une liste de l'utilisateur
     *
     * @param listid Identifiant de la liste
     * @return Liste supprimée
     * @throws SerializationException
     * @throws ServiceException
     * @throws ServiceNotAvailable
     */
    public ListDTO removeListFromUser(long listid) throws SerializationException, ServiceException, ServiceNotAvailable {
        String url = null;
        ListDTO list = null;
        try {
            url = webServiceContext.getUrl() + "users/" + webServiceContext.getPerson().getId() + "/" + getWebserviceEntityCollectionName() + "/" + listid;
            HttpDelete request = new HttpDelete(url);
            initializeAuthentification(request);
            JsonObjectMapper<ListDTO> listMapper = new JsonObjectMapper<ListDTO>(ListDTO.class);
            HttpResponse response = client.execute(request);
            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }

            list = (ListDTO) listMapper.read(responseContent);
        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }

        return list;
    }

    /**
     * Récupère la liste des articles dans la liste
     *
     * @param listid Identifiant de la liste
     * @return Liste des articles dans la liste
     * @throws SerializationException
     * @throws ServiceException
     * @throws ServiceNotAvailable
     */
    public List<ListReferenceDTO> getListReferencesOfList(long listid) throws SerializationException, ServiceException, ServiceNotAvailable {
        String url = null;
        List<ListReferenceDTO> listReferences = null;
        try {
            url = webServiceContext.getUrl() + "users/" + webServiceContext.getPerson().getId() + "/" + getWebserviceEntityCollectionName() + "/" + listid + "/listReferences";
            HttpGet request = new HttpGet(url);
            initializeAuthentification(request);
            HttpResponse response = client.execute(request);
            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }

            JsonObjectMapper<ListReferenceDTO> listReferenceMapper = new JsonObjectMapper<ListReferenceDTO>(ListReferenceDTO.class);
            listReferences = listReferenceMapper.readList(responseContent);

        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }

        return listReferences;
    }

    /**
     * Récupère un article dans une liste de courses à effectuer
     *
     * @param listid Identifiant de la liste de courses
     * @param referenceid Identifiant de l'article
     * @return Article dans la liste de courses
     * @throws SerializationException
     * @throws ServiceException
     * @throws ServiceNotAvailable
     */
    public ListReferenceDTO getListReferenceByIds(long listid, long referenceid) throws SerializationException, ServiceException, ServiceNotAvailable {
        String url = null;
        ListReferenceDTO listReference = null;
        try {
            url = webServiceContext.getUrl() + "users/" + webServiceContext.getPerson().getId() + "/" + getWebserviceEntityCollectionName()
                    + "/" + listid + "/listReferences/" + referenceid;
            HttpGet request = new HttpGet(url);
            initializeAuthentification(request);
            HttpResponse response = client.execute(request);
            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }

            JsonObjectMapper<ListReferenceDTO> listReferenceMapper = new JsonObjectMapper<ListReferenceDTO>(ListReferenceDTO.class);
            listReference = listReferenceMapper.read(responseContent);

        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }

        return listReference;
    }

    /**
     * Met à jour la liste des articles dans la liste
     *
     * @param listid Identfiant de la liste
     * @param toUpdate Map des articles à mettre à jour avec l'action à réaliser
     * les concernant
     * @throws SerializationException
     * @throws ServiceException
     * @throws ServiceNotAvailable
     */
    public void updateListReferencesOfList(long listid, Map<ListReferenceDTO, EActionList> toUpdate) throws SerializationException, ServiceException, ServiceNotAvailable {
        String url = null;
        try {
            url = webServiceContext.getUrl() + "users/" + webServiceContext.getPerson().getId() + "/" + getWebserviceEntityCollectionName() + "/" + listid + "/listReferences";
            HttpPut request = new HttpPut(url);
            initializeAuthentification(request);
            JsonObjectMapper<ListReferenceDTO> listReferenceMapper = new JsonObjectMapper<ListReferenceDTO>(ListReferenceDTO.class, EActionList.class);
            request.setEntity(new StringEntity(listReferenceMapper.writeMap(toUpdate)));
            HttpResponse response = client.execute(request);
            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }

        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }
    }

}
