/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservicelib.accessservice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import smartshopping.webservicelib.exceptions.SerializationException;
import smartshopping.webservicelib.exceptions.ServiceException;
import smartshopping.webservicelib.exceptions.ServiceNotAvailable;
import smartshopping.webservicelib.model.ReferenceDTO;
import smartshopping.webservicelib.util.WebserviceContext;

/**
 * Classe facilitant l'accès aux services liés aux références
 * @author FLORIAN
 */
public class ReferenceAccessService extends GenericAccessService<ReferenceDTO> {

    /**
     * Constructeur
     * @param webServiceContext  contient les informations nécessaires pour accéder aux services
     */
    public ReferenceAccessService(WebserviceContext webServiceContext) {
        super(webServiceContext);
    }

    /**
     * Ajoute une référence
     * @param reference Référence à ajouter
     * @param brandid Identifiant de la marque
     * @return Référence mise à jour 
     * @throws ServiceException
     * @throws ServiceNotAvailable
     * @throws SerializationException 
     */
    public ReferenceDTO add(ReferenceDTO reference, long brandid) throws ServiceException, ServiceNotAvailable, SerializationException {
        String url = null;
        try {
            url = webServiceContext.getUrl() + getWebserviceEntityCollectionName() + "/";
            HttpPost request = new HttpPost(url);
            initializeAuthentification(request);
            List<NameValuePair> values = new ArrayList<NameValuePair>(2);
            values.add(new BasicNameValuePair("serialized", jsonMapper.write(reference)));
            values.add(new BasicNameValuePair("brandid", String.valueOf(brandid)));
            UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(values, "UTF-8");
            request.setHeader("Content-Type", "application/x-www-form-urlencoded");
            request.setEntity(formEntity);
            HttpResponse response = client.execute(request);
            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 201) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }

            reference = (ReferenceDTO) jsonMapper.read(responseContent);
        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }

        return reference;
    }
}
