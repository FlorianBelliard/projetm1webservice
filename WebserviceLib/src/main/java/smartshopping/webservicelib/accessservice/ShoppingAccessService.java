/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservicelib.accessservice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;
import smartshopping.webservicelib.exceptions.SerializationException;
import smartshopping.webservicelib.exceptions.ServiceException;
import smartshopping.webservicelib.exceptions.ServiceNotAvailable;
import smartshopping.webservicelib.model.ShoppingDTO;
import smartshopping.webservicelib.model.ShoppingItemDTO;
import smartshopping.webservicelib.serialization.JsonObjectMapper;
import smartshopping.webservicelib.util.WebserviceContext;

/**
 * Classe facilitant l'accès aux services liés aux listes de courses effectuées
 *
 * @author FLORIAN
 */
public class ShoppingAccessService extends GenericAccessService<ShoppingDTO> {

    /**
     * Constructeur
     *
     * @param webserviceContext contient les informations nécessaires pour
     * accéder aux services
     */
    public ShoppingAccessService(WebserviceContext webserviceContext) {
        super(webserviceContext);
    }

    /**
     * Crée une liste de courses à effectuer dans le magasin choisi
     *
     * @param listid Identifiant de la liste à effectuer
     * @param storeid Identifiant du magasin
     * @return Liste de courses à effectuer
     * @throws SerializationException
     * @throws ServiceException
     * @throws ServiceNotAvailable
     */
    public ShoppingDTO doShopping(long listid, long storeid) throws SerializationException, ServiceException, ServiceNotAvailable {
        String url = null;
        ShoppingDTO shopping = null;

        try {
            List<NameValuePair> values = new ArrayList<NameValuePair>();
            values.add(new BasicNameValuePair("storeid", String.valueOf(storeid)));

            url = webServiceContext.getUrl() + "users/" + webServiceContext.getPerson().getId() + "/lists/" + listid + "/shopping?" + URLEncodedUtils.format(values, "UTF-8");

            HttpPost request = new HttpPost(url);
            initializeAuthentification(request);
            HttpResponse response = client.execute(request);
            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 201) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }
            JsonObjectMapper<ShoppingDTO> shoppingMapper = new JsonObjectMapper<ShoppingDTO>(ShoppingDTO.class);
            shopping = (ShoppingDTO) shoppingMapper.read(responseContent);
        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }

        return shopping;
    }

    /**
     * Récupère la liste des articles dans la liste de courses à effectuer
     *
     * @param listid Identifiant de la liste
     * @return Liste des articles
     * @throws SerializationException
     * @throws ServiceException
     * @throws ServiceNotAvailable
     */
    public List<ShoppingItemDTO> getShoppingItemsOfShopping(long listid) throws SerializationException, ServiceException, ServiceNotAvailable {
        String url = null;
        List<ShoppingItemDTO> shoppingItems = null;
        try {
            url = webServiceContext.getUrl() + "users/" + webServiceContext.getPerson().getId() + "/lists/" + listid + "/shopping/shoppingItems";
            HttpGet request = new HttpGet(url);
            initializeAuthentification(request);
            HttpResponse response = client.execute(request);
            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }

            JsonObjectMapper<ShoppingItemDTO> shoppingItemMapper = new JsonObjectMapper<ShoppingItemDTO>(ShoppingItemDTO.class);
            shoppingItems = shoppingItemMapper.readList(responseContent);

        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }

        return shoppingItems;
    }

    /**
     * Récupère un article dans une liste de courses à effectuer
     *
     * @param listid Identifiant de la liste de courses
     * @param itemid Identifiant de l'article
     * @return Article dans la liste de courses
     * @throws SerializationException
     * @throws ServiceException
     * @throws ServiceNotAvailable
     */
    public ShoppingItemDTO getShoppingItemByIds(long listid, long itemid) throws SerializationException, ServiceException, ServiceNotAvailable {
        String url = null;
        ShoppingItemDTO shoppingItem = null;
        try {
            url = webServiceContext.getUrl() + "users/" + webServiceContext.getPerson().getId() + "/lists/" + listid + "/shopping/shoppingItems/" + itemid;
            HttpGet request = new HttpGet(url);
            initializeAuthentification(request);
            HttpResponse response = client.execute(request);
            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }

            JsonObjectMapper<ShoppingItemDTO> shoppingItemMapper = new JsonObjectMapper<ShoppingItemDTO>(ShoppingItemDTO.class);
            shoppingItem = shoppingItemMapper.read(responseContent);

        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }

        return shoppingItem;
    }

    /**
     * Supprime une liste de courses à effectuer dans un magasin
     *
     * @param listid Identifiant de la liste
     * @return Liste supprimée
     * @throws SerializationException
     * @throws ServiceException
     * @throws ServiceNotAvailable
     */
    public ShoppingDTO deleteShoppingForList(long listid) throws SerializationException, ServiceException, ServiceNotAvailable {
        String url = null;
        ShoppingDTO shopping = null;
        try {
            url = webServiceContext.getUrl() + "users/" + webServiceContext.getPerson().getId() + "/lists/" + listid + "/shopping";
            HttpDelete request = new HttpDelete(url);
            initializeAuthentification(request);
            JsonObjectMapper<ShoppingDTO> shoppingMapper = new JsonObjectMapper<ShoppingDTO>(ShoppingDTO.class);
            HttpResponse response = client.execute(request);
            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }

            shopping = (ShoppingDTO) shoppingMapper.read(responseContent);
        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }

        return shopping;
    }

    /**
     * Met à jour la liste des articles dans la liste à effectuer
     *
     * @param listid Identfiant de la liste
     * @param toUpdate Map des articles à mettre à jour avec l'action à réaliser
     * les concernant
     * @throws SerializationException
     * @throws ServiceException
     * @throws ServiceNotAvailable
     */
    public void updateShoppingItemsOfList(long listid, Map<ShoppingItemDTO, EActionList> toUpdate) throws SerializationException, ServiceException, ServiceNotAvailable {
        String url = null;
        try {
            url = webServiceContext.getUrl() + "users/" + webServiceContext.getPerson().getId() + "/lists/" + listid + "/shopping/shoppingItems";
            HttpPut request = new HttpPut(url);
            initializeAuthentification(request);
            JsonObjectMapper<ShoppingItemDTO> shoppingItemMapper = new JsonObjectMapper<ShoppingItemDTO>(ShoppingItemDTO.class, EActionList.class);
            request.setEntity(new StringEntity(shoppingItemMapper.writeMap(toUpdate)));
            HttpResponse response = client.execute(request);
            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }

        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }
    }
}
