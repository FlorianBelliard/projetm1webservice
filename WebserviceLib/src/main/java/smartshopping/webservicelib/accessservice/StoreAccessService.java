/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservicelib.accessservice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.message.BasicNameValuePair;
import smartshopping.webservicelib.exceptions.SerializationException;
import smartshopping.webservicelib.exceptions.ServiceException;
import smartshopping.webservicelib.exceptions.ServiceNotAvailable;
import smartshopping.webservicelib.model.StoreDTO;
import smartshopping.webservicelib.util.WebserviceContext;

/**
 * Classe facilitant l'accès aux services liés aux magasins
 * @author Alexis
 */
public class StoreAccessService extends GenericAccessService<StoreDTO> {

    /**
     * Constructeur
     * @param webServiceContext contient les informations nécessaires pour accéder aux services
     */
    public StoreAccessService(WebserviceContext webServiceContext) {
        super(webServiceContext);
    }

    /**
     * Ajoute un magasin 
     * @param store Magasin à ajouter
     * @param chainid Identifiant de la chaîne à laquelle le magasin va être rattachée
     * @return Magasin ajouté
     * @throws ServiceException
     * @throws ServiceNotAvailable
     * @throws SerializationException 
     */
    public StoreDTO add(StoreDTO store, long chainid) throws ServiceException, ServiceNotAvailable, SerializationException {
        String url = null;
        try {
            url = webServiceContext.getUrl() + getWebserviceEntityCollectionName() + "/";
            HttpPost request = new HttpPost(url);
            initializeAuthentification(request);
            List<NameValuePair> values = new ArrayList<NameValuePair>(2);
            values.add(new BasicNameValuePair("serialized", jsonMapper.write(store)));
            values.add(new BasicNameValuePair("chainid", String.valueOf(chainid)));
            UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(values, "UTF-8");
            request.setHeader("Content-Type", "application/x-www-form-urlencoded");
            request.setEntity(formEntity);
            HttpResponse response = client.execute(request);
            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 201) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }

            store = (StoreDTO) jsonMapper.read(responseContent);
        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }

        return store;
    }

    /**
     * Met à jour un magasin 
     * @param store Magasin
     * @param chainid Identifiant de la chaîne à laquelle est rattaché le magasin
     * @return Magasin mis à jour
     * @throws ServiceException
     * @throws ServiceNotAvailable
     * @throws SerializationException 
     */
    public StoreDTO update(StoreDTO store, long chainid) throws ServiceException, ServiceNotAvailable, SerializationException {
        String url = null;
        try {
            url = webServiceContext.getUrl() + getWebserviceEntityCollectionName() + "/";
            HttpPut request = new HttpPut(url);
            initializeAuthentification(request);
            List<NameValuePair> values = new ArrayList<NameValuePair>(2);
            values.add(new BasicNameValuePair("serialized", jsonMapper.write(store)));
            values.add(new BasicNameValuePair("chainid", String.valueOf(chainid)));
            UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(values, "UTF-8");
            request.setHeader("Content-Type", "application/x-www-form-urlencoded");
            request.setEntity(formEntity);
            HttpResponse response = client.execute(request);
            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 201) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }

            store = (StoreDTO) jsonMapper.read(responseContent);
        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }

        return store;
    }
}
