/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservicelib.accessservice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import smartshopping.webservicelib.exceptions.SerializationException;
import smartshopping.webservicelib.exceptions.ServiceException;
import smartshopping.webservicelib.exceptions.ServiceNotAvailable;
import smartshopping.webservicelib.model.StoreDTO;
import smartshopping.webservicelib.model.UserDTO;
import smartshopping.webservicelib.serialization.JsonObjectMapper;
import smartshopping.webservicelib.util.WebserviceContext;

/**
 * Classe permettant de faciliter l'accès aux services liés aux utilisateurs
 *
 * @author FLORIAN
 */
public class UserAccessService extends GenericAccessService<UserDTO> {

    /**
     * Constructeur
     *
     * @param webServiceContext Contient les différentes informations pour
     * accéder aux services
     */
    public UserAccessService(WebserviceContext webServiceContext) {
        super(webServiceContext);
    }

    /**
     * Indique si un utilisateur existe
     *
     * @param login Login de l'utilisateur
     * @param password Mot de passe de l'utilisateur
     * @return Utilisateur trouvé
     * @throws SerializationException
     * @throws ServiceNotAvailable
     * @throws ServiceException
     */
    public UserDTO isUserExists(String login, String password) throws SerializationException, ServiceNotAvailable, ServiceException {
        UserDTO user = null;
        String url = null;
        try {
            url = webServiceContext.getUrl() + getWebserviceEntityCollectionName() + "/exists";
            HttpPost request = new HttpPost(url);
            List<NameValuePair> values = new ArrayList<NameValuePair>(2);
            values.add(new BasicNameValuePair("login", login));
            values.add(new BasicNameValuePair("password", password));
            UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(values, "UTF-8");
            request.setHeader("Content-Type", "application/x-www-form-urlencoded");
            request.setEntity(formEntity);
            HttpResponse response = client.execute(request);
            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }

            if (responseContent.isEmpty()) {
                user = null;
            } else {
                user = (UserDTO) jsonMapper.read(responseContent);
            }
        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }

        return user;
    }

    /**
     * Récupère la liste des magasins favoris de l'utilisateur
     *
     * @return Liste des magasins
     * @throws SerializationException
     * @throws ServiceNotAvailable
     * @throws ServiceException
     */
    public List<StoreDTO> getFavoritStores() throws SerializationException, ServiceNotAvailable, ServiceException {
        List<StoreDTO> stores = null;
        String url = null;

        try {
            url = webServiceContext.getUrl() + getWebserviceEntityCollectionName() + "/" + webServiceContext.getPerson().getId() + "/favoriteStores";
            HttpGet request = new HttpGet(url);
            initializeAuthentification(request);
            HttpResponse response = client.execute(request);
            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }

            JsonObjectMapper<StoreDTO> storeMapper = new JsonObjectMapper<StoreDTO>(StoreDTO.class);
            stores = storeMapper.readList(responseContent);
        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }

        return stores;
    }

    /**
     * Ajoute un magasin aux favoris de l'utilisateur
     *
     * @param storeid Identifiant du magasin
     * @return Magasin ajouté aux favoris
     * @throws SerializationException
     * @throws ServiceNotAvailable
     * @throws ServiceException
     */
    public StoreDTO addFavoriteStoreToUser(long storeid) throws SerializationException, ServiceNotAvailable, ServiceException {
        StoreDTO store = null;
        String url = null;

        try {
            url = webServiceContext.getUrl() + getWebserviceEntityCollectionName() + "/" + webServiceContext.getPerson().getId() + "/favoriteStores";
            HttpPost request = new HttpPost(url);
            initializeAuthentification(request);
            List<NameValuePair> values = new ArrayList<NameValuePair>(1);
            values.add(new BasicNameValuePair("storeid", String.valueOf(storeid)));
            UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(values, "UTF-8");
            request.setHeader("Content-Type", "application/x-www-form-urlencoded");
            request.setEntity(formEntity);
            HttpResponse response = client.execute(request);

            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 201) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }

            JsonObjectMapper<StoreDTO> storeMapper = new JsonObjectMapper<StoreDTO>(StoreDTO.class);
            store = storeMapper.read(responseContent);
        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }

        return store;
    }

    /**
     * Supprime un magasin des favoris de l'utilisateur
     *
     * @param storeid Identifiant du magasin
     * @return Magasin supprimé des favoris
     * @throws SerializationException
     * @throws ServiceException
     * @throws ServiceNotAvailable
     */
    public StoreDTO removeFavoriteStoreFromUser(long storeid) throws SerializationException, ServiceException, ServiceNotAvailable {
        String url = null;
        StoreDTO store = null;
        try {
            url = webServiceContext.getUrl() + getWebserviceEntityCollectionName() + "/" + webServiceContext.getPerson().getId() + "/favoriteStores/" + storeid;
            HttpDelete request = new HttpDelete(url);
            initializeAuthentification(request);
            JsonObjectMapper<StoreDTO> storeMapper = new JsonObjectMapper<StoreDTO>(StoreDTO.class);
            HttpResponse response = client.execute(request);
            String responseContent = convertStreamToString(response.getEntity().getContent());

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new ServiceException(responseContent, response.getStatusLine().getStatusCode());
            }

            store = (StoreDTO) storeMapper.read(responseContent);
        } catch (IOException ex) {
            throw new ServiceNotAvailable(ex.getLocalizedMessage(), url, ex);
        }

        return store;
    }

}
