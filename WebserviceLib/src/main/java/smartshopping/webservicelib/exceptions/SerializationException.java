/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservicelib.exceptions;

/**
 * Exception lancée par la sérialisation de Jackson
 *
 * @author FLORIAN
 */
public class SerializationException extends Exception {

    public SerializationException(String message) {
        super("Une erreur est survenue durant la sérialisation/désérialisation :" + message);
    }

    public SerializationException(String message, Throwable e) {
        super("Une erreur est survenue durant la sérialisation/désérialisation :" + message, e);
    }
}
