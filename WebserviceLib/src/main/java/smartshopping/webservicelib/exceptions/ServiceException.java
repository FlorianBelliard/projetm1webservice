/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservicelib.exceptions;

/**
 *
 * @author FLORIAN
 */
public class ServiceException extends Exception {

    public ServiceException(String message, int code) {
        super("L'appel au webservice a rencontré une erreur " + code + " : " + message);
    }
}
