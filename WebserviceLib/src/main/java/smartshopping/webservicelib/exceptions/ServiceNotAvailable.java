/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservicelib.exceptions;

/**
 *
 * @author FLORIAN
 */
public class ServiceNotAvailable extends Exception {

    public ServiceNotAvailable(String message, String url) {
        super("Impossible d'accéder au webservice à l'adresse " + url + " :" + message);
    }

    public ServiceNotAvailable(String message, String url, Throwable e) {
        super("Impossible d'accéder au webservice à l'adresse " + url + " :" + message, e);
    }
}
