/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservicelib.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Classe représentant une marque de produits
 *
 * @author FLORIAN
 */
public class BrandDTO extends DTO implements Serializable {

    /**
     * Libellé de la marque
     */
    private String label;

    /**
     * Constructeur par défaut
     */
    public BrandDTO() {
    }

    /**
     * Constructeur avec paramètres
     *
     * @param label Libellé
     */
    public BrandDTO(String label) {
        this.label = label;
    }

    /**
     * Obtient le libellé de la marque
     *
     * @return Libellé
     */
    public String getLabel() {
        return label;
    }

    /**
     * Définit le libellé de la marque
     *
     * @param label Libellé
     */
    public void setLabel(String label) {
        this.label = label;
    }
}
