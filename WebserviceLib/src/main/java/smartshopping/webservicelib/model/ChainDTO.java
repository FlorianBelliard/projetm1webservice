/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservicelib.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Classe répresentant une chaîne de magasins
 *
 * @author FLORIAN
 */
public class ChainDTO extends DTO implements Serializable {

    /**
     * Libellé de la chaîne
     */
    private String label;

    /**
     * Constructeur par défaut
     */
    public ChainDTO() {
    }

    /**
     * Constrcuteur avec paramètres
     *
     * @param label Libellé
     */
    public ChainDTO(String label, Set<BrandDTO> brands, Set<StoreDTO> stores) {
        this.label = label;
    }

    /**
     * Obtient le libellé de la chaîne
     *
     * @return Libellé
     */
    public String getLabel() {
        return label;
    }

    /**
     * Définit le libellé de la chaîne
     *
     * @param label Libellé
     */
    public void setLabel(String label) {
        this.label = label;
    }
}
