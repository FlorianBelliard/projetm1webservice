/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservicelib.model;

/**
 *
 * @author FLORIAN
 */
public class DTO {

    /**
     * Identifiant du DTO
     */
    private long id;

    /**
     * Obtient l'identifiant du DTO
     *
     * @return Identifiant
     */
    public long getId() {
        return id;
    }

    /**
     * Définit l'identifiant du DTO
     *
     * @param id Identifiant
     */
    public void setId(long id) {
        this.id = id;
    }
}
