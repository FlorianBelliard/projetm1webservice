/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservicelib.model;

import java.io.Serializable;

/**
 *
 * @author FLORIAN
 */
public class ItemDTO extends DTO implements Serializable {

    /**
     * Prix de l'article
     */
    private double price;

    /**
     * Magasin auquel appartient l'article
     */
    private StoreDTO store;

    /**
     * Référence de l'article
     */
    private ReferenceDTO reference;

    /**
     * Constructeur par défaut
     */
    public ItemDTO() {
    }

    /**
     * Constructeur avec paramètres
     *
     * @param price Prix
     * @param store Magasin
     */
    public ItemDTO(double price, StoreDTO store, ReferenceDTO reference) {
        this.price = price;
        this.store = store;
        this.reference = reference;
    }

    /**
     * Obtient le prix de l'article
     *
     * @return Prix
     */
    public double getPrice() {
        return price;
    }

    /**
     * Définit le prix de l'article
     *
     * @param price Prix
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * Obtient le magasin auquel appartient l'article
     *
     * @return Magasin
     */
    public StoreDTO getStore() {
        return store;
    }

    /**
     * Définit le magasin auquel appartient l'article
     *
     * @param store Magasin
     */
    public void setStore(StoreDTO store) {
        this.store = store;
    }

    /**
     * Obtient la référence de l'article
     *
     * @return ReferenceDTO
     */
    public ReferenceDTO getReference() {
        return reference;
    }

    /**
     * Définit la référence de l'article
     *
     * @param reference Référence
     */
    public void setReference(ReferenceDTO reference) {
        this.reference = reference;
    }

}
