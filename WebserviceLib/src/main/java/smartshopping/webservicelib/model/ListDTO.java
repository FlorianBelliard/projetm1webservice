/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservicelib.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Classe réprésentant une liste de courses
 *
 * @author FLORIAN
 */
public class ListDTO extends DTO implements Serializable {

    /**
     * Libellé de la liste
     */
    private String label;

    /**
     * Utilisateur à qui appartient la liste
     */
    private UserDTO user;

    /**
     * ListDTOe de courses à effectuer ou effectuée
     */
    private ShoppingDTO shopping;
    
    /**
     * Liste des références contenues dans la liste
     */
    private Set<ListReferenceDTO> listReferences = new HashSet<ListReferenceDTO>(0);

    /**
     * Constructeur par défaut
     */
    public ListDTO() {
    }

    /**
     * Constructeur avec paramètres
     *
     * @param label Libellé
     * @param user Utilsiateur
     * @param shopping Courses
     */
    public ListDTO(String label, UserDTO user, ShoppingDTO shopping) {
        this.label = label;
        this.user = user;
        this.shopping = shopping;
    }

    /**
     * Obtient le libellé de la liste
     *
     * @return Libellé
     */
    public String getLabel() {
        return label;
    }

    /**
     * Définit le libellé de la liste
     *
     * @param label Libellé
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Obtient l'utilisateur à qui appartient la liste
     *
     * @return Utilisateur
     */
    public UserDTO getUser() {
        return user;
    }

    /**
     * Définit l'utilisateur à qui appartient la liste
     *
     * @param user Utilisateur
     */
    public void setUser(UserDTO user) {
        this.user = user;
    }

    /**
     * Obtient les courses à effectuer de la liste
     *
     * @return Courses à effectuer
     */
    public ShoppingDTO getShopping() {
        return shopping;
    }

    /**
     * Définit les courses à effectuer de la liste
     *
     * @param shopping Course à effectuer
     */
    public void setShopping(ShoppingDTO shopping) {
        this.shopping = shopping;
    }

    /**
     * Obtient la liste des références dans la liste
     * @return Liste des références
     */
    public Set<ListReferenceDTO> getListReferences() {
        return listReferences;
    }

    /**
     * Définit la liste des références dans la liste
     * @param listReferences 
     */
    public void setListReferences(Set<ListReferenceDTO> listReferences) {
        this.listReferences = listReferences;
    }
}
