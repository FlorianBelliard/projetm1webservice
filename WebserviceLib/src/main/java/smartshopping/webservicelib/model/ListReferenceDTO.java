/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservicelib.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import smartshopping.webservicelib.exceptions.SerializationException;
import smartshopping.webservicelib.model.interfaces.IAssociationDTO;
import smartshopping.webservicelib.serialization.JsonObjectMapper;

/**
 * Classe réprésentant le fait qu'une référence soit dans une liste
 *
 * @author FLORIAN
 */
public class ListReferenceDTO extends DTO implements Serializable, IAssociationDTO {

    /**
     * Quantité de la référence dans la liste
     */
    private int quantity;

    /**
     * Liste
     */
    private ListDTO list;

    /**
     * Référence
     */
    private ReferenceDTO reference;

    /**
     * Constructeur par défaut
     */
    public ListReferenceDTO() {
    }

    /**
     * Obtient la liste
     *
     * @return ListDTOe
     */
    public ListDTO getList() {
        return list;
    }

    /**
     * Définit la liste
     *
     * @param list ListDTOe
     */
    public void setList(ListDTO list) {
        this.list = list;
    }

    /**
     * Obtient la référence
     *
     * @return Référence
     */
    public ReferenceDTO getReference() {
        return reference;
    }

    /**
     * Définit la référence
     *
     * @param reference
     */
    public void setReference(ReferenceDTO reference) {
        this.reference = reference;
    }

    /**
     * Définit la quantité de la référence dans la liste
     *
     * @return Quantité
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Définit la quantité de la référence dans la liste
     *
     * @param quantity Quantité
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @JsonIgnore
    @Override
    public long getId1() {
        return list != null ? list.getId() : 0;
    }

    @JsonIgnore
    @Override
    public long getId2() {
        return reference != null ? reference.getId() : 0;
    }
    
    @JsonCreator
    public static ListReferenceDTO create(String jsonString){
        ListReferenceDTO listReference = null;
        try {
            JsonObjectMapper<ListReferenceDTO> mapper = new JsonObjectMapper<ListReferenceDTO>(ListReferenceDTO.class);
            
            listReference = mapper.read(jsonString);
        } catch (SerializationException ex) {
            Logger.getLogger(ListReferenceDTO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return listReference;
    }
    
    @Override
    public String toString(){
        String toString = null;
        
        try {
            JsonObjectMapper<ListReferenceDTO> mapper = new JsonObjectMapper<ListReferenceDTO>(ListReferenceDTO.class);
            toString = mapper.write(this);
        } catch (SerializationException ex) {
            Logger.getLogger(ListReferenceDTO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return toString;
    }
}
