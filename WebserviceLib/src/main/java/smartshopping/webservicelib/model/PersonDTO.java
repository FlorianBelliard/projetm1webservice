/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservicelib.model;

import java.io.Serializable;

/**
 * Classe abstraite représentant une personne
 * @author FLORIAN
 */
public abstract class PersonDTO extends DTO implements Serializable {

    /**
     * Login de l'utilisateur
     */
    protected String login;

    /**
     * Mot de passe de l'utilisateur
     */
    protected String password;

    /**
     * Obtient le login de l'utilisateur
     *
     * @return Login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Définit le login de l'utilisateur
     *
     * @param login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Obtient le mot de passe de l'utilisateur
     *
     * @return Mot de passe
     */
    public String getPassword() {
        return password;
    }

    /**
     * Définit le mot de passe de l'utilisateur
     *
     * @param password Mot de passe
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    
}
