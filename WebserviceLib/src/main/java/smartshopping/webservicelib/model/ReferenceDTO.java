/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservicelib.model;

import java.io.Serializable;

/**
 * Classe réprésentant une référence de produit
 *
 * @author FLORIAN
 */
public class ReferenceDTO extends DTO implements Serializable {

    /**
     * Code de la référence
     */
    private String code;

    /**
     * Libellé de la référence
     */
    private String label;

    /**
     * Marque associée à la référence
     */
    private BrandDTO brand;

    /**
     * Constructeur par défaut
     */
    public ReferenceDTO() {
    }

    /**
     * Constructeur avec paramètres
     *
     * @param label Libellé
     * @param brand Marque
     */
    public ReferenceDTO(String code, String label, BrandDTO brand) {
        this.label = label;
        this.brand = brand;
    }

    /**
     * Obtient le code de la référence
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     * Définit le code de la référence
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Obtient le libellé de la référence
     *
     * @return Libellé
     */
    public String getLabel() {
        return label;
    }

    /**
     * Définit le libellé de la référence
     *
     * @param label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Obtien la marque associée à la référence
     *
     * @return Marque
     */
    public BrandDTO getBrand() {
        return brand;
    }

    /**
     * Définit la marque associée à la référence
     *
     * @param brand Marque
     */
    public void setBrand(BrandDTO brand) {
        this.brand = brand;
    }
}
