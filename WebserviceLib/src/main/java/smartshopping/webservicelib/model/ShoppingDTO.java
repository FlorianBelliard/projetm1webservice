/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservicelib.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Classe réprésentant les courses à effectuer
 *
 * @author FLORIAN
 */
public class ShoppingDTO extends DTO implements Serializable {

    /**
     * Statut des courses
     */
    private Boolean status;

    /**
     * Prix des courses
     */
    private double price;

    /**
     * Date des courses
     */
    private Date date;

    /**
     * ListDTOe associée aux courses
     */
    private ListDTO list;

    /**
     * Liste des articles présents dans la liste de courses
     */
    private Set<ShoppingItemDTO> shoppingItems = new HashSet<ShoppingItemDTO>(0);

    /**
     * Magasin dans lequel on effectue la course
     */
    private StoreDTO store;

    /**
     * Constructeur par défaut
     */
    public ShoppingDTO() {
    }

    /**
     * Constructeur avec paramètres
     *
     * @param status Statut
     * @param date Date
     * @param price Prix
     * @param list ListDTOe
     */
    public ShoppingDTO(Boolean status, Date date, double price, ListDTO list) {
        this.status = status;
        this.date = date;
        this.price = price;
        this.list = list;
    }

    /**
     * Obtient le statut des courses
     *
     * @return Statut
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * Définit le statut des courses
     *
     * @param status Statut
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * Obtient la date des courses
     *
     * @return Date des courses
     */
    public Date getDate() {
        return date;
    }

    /**
     * Définit la date des courses
     *
     * @param date Date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Obtient le prix des courses
     *
     * @return Prix
     */
    public double getPrice() {
        return price;
    }

    /**
     * Définit le prix des courses
     *
     * @param price Prix
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * Obtient la liste auquelle correspond les courses
     *
     * @return ListDTOe
     */
    public ListDTO getList() {
        return list;
    }

    /**
     * Définit la liste auquelle correspond les courses
     *
     * @param list ListDTOe
     */
    public void setList(ListDTO list) {
        this.list = list;
    }

    /**
     * Obtient les articles présents dans les courses
     *
     * @return Liste des articles
     */
    public Set<ShoppingItemDTO> getShoppingItems() {
        return shoppingItems;
    }

    /**
     * Définit les articles présents dans les courses
     *
     * @param shoppingItems Liste des articles
     */
    public void setShoppingItems(Set<ShoppingItemDTO> shoppingItems) {
        this.shoppingItems = shoppingItems;
    }

    /**
     * Obtient le magasin de la course
     *
     * @return
     */
    public StoreDTO getStore() {
        return store;
    }

    /**
     * Définit le magasin de la course
     *
     * @param store
     */
    public void setStore(StoreDTO store) {
        this.store = store;
    }

}
