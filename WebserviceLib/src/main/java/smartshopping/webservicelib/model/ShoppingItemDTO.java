/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservicelib.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import smartshopping.webservicelib.exceptions.SerializationException;
import smartshopping.webservicelib.model.interfaces.IAssociationDTO;
import smartshopping.webservicelib.serialization.JsonObjectMapper;

/**
 * Classe réprésentant le fait qu'un article soit dans une course
 *
 * @author FLORIAN
 */
public class ShoppingItemDTO extends DTO implements Serializable, IAssociationDTO {

    /**
     * Quantité de l'article dans les courses
     */
    private int quantity;

    /**
     * Statut de l'article dans les courses
     */
    private Boolean status;

    /**
     * Shopping
     */
    private ShoppingDTO shopping;

    /**
     * Item
     */
    private ItemDTO item;

    /**
     * Constructeur par défaut
     */
    public ShoppingItemDTO() {
    }

    /**
     * Obtient les courses
     *
     * @return Courses
     */
    public ShoppingDTO getShopping() {
        return shopping;
    }

    /**
     * Définit les courses
     *
     * @param shopping Courses
     */
    public void setShopping(ShoppingDTO shopping) {
        this.shopping = shopping;
    }

    /**
     * Obtient l'article
     *
     * @return Article
     */
    public ItemDTO getItem() {
        return item;
    }

    /**
     * Définit l'article
     *
     * @param item Article
     */
    public void setItem(ItemDTO item) {
        this.item = item;
    }

    /**
     * Obtient la quantité de l'article dans les courses
     *
     * @return Quantité
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Définit la quantité de l'article dans les courses
     *
     * @param quantity Quantité
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * Obtient le statut d'un article dans la liste de courses
     *
     * @return Statut
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * Définit le statut d'un article dans la liste des courses
     *
     * @param status Statut
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    @JsonIgnore
    @Override
    public long getId1() {
        return shopping != null ? shopping.getId() : 0;
    }

    @JsonIgnore
    @Override
    public long getId2() {
        return item != null ? item.getId() : 0;
    }

    @JsonCreator
    public static ShoppingItemDTO create(String jsonString) {
        ShoppingItemDTO shoppingItem = null;
        try {
            JsonObjectMapper<ShoppingItemDTO> mapper = new JsonObjectMapper<ShoppingItemDTO>(ShoppingItemDTO.class);

            shoppingItem = mapper.read(jsonString);
        } catch (SerializationException ex) {
            Logger.getLogger(ShoppingItemDTO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return shoppingItem;
    }

    @Override
    public String toString() {
        String toString = null;

        try {
            JsonObjectMapper<ShoppingItemDTO> mapper = new JsonObjectMapper<ShoppingItemDTO>(ShoppingItemDTO.class);
            toString = mapper.write(this);
        } catch (SerializationException ex) {
            Logger.getLogger(ShoppingItemDTO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return toString;
    }
}
