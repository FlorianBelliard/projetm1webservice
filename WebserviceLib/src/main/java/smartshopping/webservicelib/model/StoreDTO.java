/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservicelib.model;

import java.io.Serializable;

/**
 * Classe représentant un magasin
 *
 * @author FLORIAN
 */
public class StoreDTO extends DTO implements Serializable {

    /**
     * Libellé du magasin
     */
    private String label;

    /**
     * Adresse du magasin
     */
    private String address;

    /**
     * Position GPS du magasin
     */
    private String position;

    /**
     * Chaîne auquelle appartient le magasin
     */
    private ChainDTO chain;

    /**
     * Constructeur par défaut
     */
    public StoreDTO() {
    }

    /**
     * Constructeur avec paramètres
     *
     * @param label Libellé
     * @param address Adresse
     * @param position Position
     */
    public StoreDTO(String label, String address, String position) {
        this.label = label;
        this.address = address;
        this.position = position;
    }

    /**
     * Obtient le libellé du magasin
     *
     * @return Libellé
     */
    public String getLabel() {
        return label;
    }

    /**
     * Définit le libellé du magasin
     *
     * @param label Libellé
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Obtient l'adresse du magasin
     *
     * @return Adresse
     */
    public String getAddress() {
        return address;
    }

    /**
     * Définit l'adresse du magasin
     *
     * @param address Adresse
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Obtient la position du magasin
     *
     * @return Position
     */
    public String getPosition() {
        return position;
    }

    /**
     * Définit la position du magasin
     *
     * @param position Position
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * Obtient la chaîne auquelle appartient le magasin
     *
     * @return Chaîne
     */
    public ChainDTO getChain() {
        return chain;
    }

    /**
     * Définit la chaîne auquelle appartient le magasin
     *
     * @param chain ChainDTOe
     */
    public void setChain(ChainDTO chain) {
        this.chain = chain;
    }
}
