/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservicelib.model;

import java.util.HashSet;
import java.util.Set;

/**
 * Classe réprésentant un utilisateur
 *
 * @author FLORIAN
 */
public class UserDTO extends PersonDTO {

    /**
     * Nom de l'utilisateur
     */
    private String lastName;

    /**
     * Prénom de l'utilisateur
     */
    private String firstName;

    /**
     * Adresse de l'utilisateur
     */
    private String address;

    /**
     * ListDTOe des listes de l'utilisateur
     */
    private Set<ListDTO> lists = new HashSet<ListDTO>(0);

    /**
     * ListDTOe des magasins favoris de l'utilisateur
     */
    private Set<StoreDTO> favoritesStores = new HashSet<StoreDTO>(0);

    /**
     * Constructeur par défaut
     */
    public UserDTO() {
    }

    /**
     * Constructeur avec paramètres
     *
     * @param lastName Nom
     * @param firstName Prénom
     * @param address Adresse
     * @param login Login
     * @param password Mot de passe
     * @param lists ListDTOes
     * @param favoriteStores Magasins favoris
     */
    public UserDTO(String lastName, String firstName, String address, String login, String password, Set<ListDTO> lists, Set<StoreDTO> favoriteStores) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.address = address;
        this.login = login;
        this.password = password;
        this.favoritesStores = favoriteStores;
        this.lists = lists;
    }

    /**
     * Obtient le nom de l'utilisateur
     *
     * @return Nom
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Définit le nom de l'utiliasteur
     *
     * @param lastName Nom
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Obtient le prénom de l'utilisateur
     *
     * @return Prénom
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Définit le nom de l'utilisateur
     *
     * @param firstName Prénom
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Obtient l'adresse de l'utilisateur
     *
     * @return Adresse
     */
    public String getAddress() {
        return address;
    }

    /**
     * Définit l'adresse de l'utilisateur
     *
     * @param address Adresse
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Obtient la listes des listes de l'utilisateur
     *
     * @return ListDTO des listes
     */
    public Set<ListDTO> getLists() {
        return lists;
    }

    /**
     * Définit la liste des listes de l'utilisateur
     *
     * @param lists ListDTOe des listes
     */
    public void setLists(Set<ListDTO> lists) {
        this.lists = lists;
    }

    /**
     * Obtient la liste des magasins favoris de l'utilisateur
     *
     * @return ListDTOe des magasins
     */
    public Set<StoreDTO> getFavoritesStores() {
        return favoritesStores;
    }

    /**
     * Définti la liste des magasins favoris de l'utilisateur
     *
     * @param favoritesStores ListDTOe des magasins
     */
    public void setFavoritesStores(Set<StoreDTO> favoritesStores) {
        this.favoritesStores = favoritesStores;
    }

}
