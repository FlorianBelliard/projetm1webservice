/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package smartshopping.webservicelib.model.interfaces;

/**
 * Permet d'identifier de manière unique une entité représentant une association
 * @author fbelliar
 */
public interface IAssociationDTO {
    
    /**
     * Obtient l'identifiant 1 de l'association
     * @return Identifiant
     */
    long getId1();
    
    /**
     * Obtient l'identifiant 2 de l'association
     * @return Identifiant
     */
    long getId2();
}
