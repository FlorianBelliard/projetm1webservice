/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservicelib.security;

import org.apache.commons.codec.binary.Base64;
import smartshopping.webservicelib.model.PersonDTO;

/**
 * Classe qui permet d'encoder/décoder des informations pour l'authentification Basic
 * @author FLORIAN
 */
public class BasicAuthentification {

    /**
     * Decode the basic auth and convert it to array login/password
     *
     * @param auth The string encoded authentification
     * @return The login (case 0), the password (case 1)
     */
    public static String[] decode(String auth) {
        //Replacing "Basic THE_BASE_64" to "THE_BASE_64" directly
        auth = auth.replaceFirst("[B|b]asic ", "");

        //Decode the Base64 into byte[]
        byte[] decodedBytes = Base64.decodeBase64(auth);

        //If the decode fails in any case
        if (decodedBytes == null || decodedBytes.length == 0) {
            return null;
        }

        //Now we can convert the byte[] into a splitted array :
        //	- the first one is login,
        //	- the second one password
        return new String(decodedBytes).split(":", 2);
    }

    public static String encode(PersonDTO person) {
        String toEncode = person.getLogin() + ":" + person.getPassword();
//        return Base64.encodeBase64String(toEncode.getBytes()).trim();
        return new String(Base64.encodeBase64(toEncode.getBytes()));
    }
}
