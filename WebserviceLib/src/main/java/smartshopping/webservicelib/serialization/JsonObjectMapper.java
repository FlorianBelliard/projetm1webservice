/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservicelib.serialization;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import smartshopping.webservicelib.exceptions.SerializationException;

/**
 * Classe qui permet de sérialiser et désérialiser les flux entrants et sortants du client
 * au format JSON grâce à Jackson
 * @author FLORIAN
 */
public class JsonObjectMapper<T> {

    private static final ObjectMapper mapper = new ObjectMapper();

    private final Class<T> type;
    
    private final Class mapValue;

    /**
     * Constructeur avec type en paramètre pour être générique
     * @param type Type de l'entité à séialiser ou désérialiser
     */
    public JsonObjectMapper(Class<T> type) {
        this(type, null);
    }

    /**
     * Constructeur avec type du DTO et type possible pour une Map
     * @param type
     * @param mapValue 
     */
    public JsonObjectMapper(Class<T> type, Class mapValue) {
        this.mapValue = mapValue;
        this.type = type;
    }
    /**
     * Sérialise une entité
     * @param o Entité
     * @return Chaîne contenant l'entité au format JSON
     * @throws SerializationException 
     */
    public String write(T o) throws SerializationException {
        String serialized = null;
        try {
            serialized = mapper.writeValueAsString(o);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(JsonObjectMapper.class.getName()).log(Level.SEVERE, null, ex);
            throw new SerializationException(ex.getLocalizedMessage(), ex);
        }
        return serialized;
    }

    /**
     * Sérialise une liste d'entités 
     * @param o Liste d'entités
     * @return Chaîne contenant la liste des entités au format JSON
     * @throws SerializationException 
     */
    public String writeList(List<T> o) throws SerializationException {
        String serialized = null;
        try {
            serialized = mapper.writeValueAsString(o);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(JsonObjectMapper.class.getName()).log(Level.SEVERE, null, ex);
            throw new SerializationException(ex.getLocalizedMessage(), ex);
        }
        return serialized;
    }

    /**
     * Desérialise une chaîne contenant une liste d'entités au format JSON
     * @param serializedObject Chaîne contenant une liste d'entités au format JSON
     * @return Liste d'entités
     * @throws SerializationException 
     */
    public List<T> readList(String serializedObject) throws SerializationException {
        List<T> o = null;
        try {
            TypeFactory typeFactory = mapper.getTypeFactory();
            o = (List<T>) mapper.readValue(serializedObject, typeFactory.constructCollectionType(List.class, type));
        } catch (IOException ex) {
            Logger.getLogger(JsonObjectMapper.class.getName()).log(Level.SEVERE, null, ex);
            throw new SerializationException(ex.getLocalizedMessage(), ex);

        }
        return o;
    }

    /**
     * Desérialise une chaîne contenant une entité au format JSON
     * @param serializedObject Chaîne contenant une entité au format JSON
     * @return Entité
     * @throws SerializationException 
     */
    public T read(String serializedObject) throws SerializationException {
        T o = null;
        try {
            o = (T) mapper.readValue(serializedObject, type);
        } catch (IOException ex) {
            Logger.getLogger(JsonObjectMapper.class.getName()).log(Level.SEVERE, null, ex);
            throw new SerializationException(ex.getLocalizedMessage(), ex);
        }
        return o;
    }
    
    /**
     * Desérialise une chaîne contenant une map d'entités au format JSON 
     * @param serializedObject Chaîne contenant une map d'entités au format JSON
     * @return Map d'entités
     * @throws SerializationException 
     */
    public Map<T, ? extends Object> readMap(String serializedObject) throws SerializationException {
        Map<T, ? extends Object> o = null;
        try {
            TypeFactory typeFactory = mapper.getTypeFactory();
            o = (Map<T, ? extends Object>) mapper.readValue(serializedObject, typeFactory.constructMapType(Map.class, type, mapValue));
        } catch (IOException ex) {
            Logger.getLogger(JsonObjectMapper.class.getName()).log(Level.SEVERE, null, ex);
            throw new SerializationException(ex.getLocalizedMessage(), ex);

        }
        return o;
    }
    
    /**
     * Sérialise une map d'entités 
     * @param o Map d'entités
     * @return Chaîne contenant la map des entités au format JSON
     * @throws SerializationException 
     */
    public String writeMap(Map<T, ? extends Object> o) throws SerializationException {
        String serialized = null;
        try {
            serialized = mapper.writeValueAsString(o);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(JsonObjectMapper.class.getName()).log(Level.SEVERE, null, ex);
            throw new SerializationException(ex.getLocalizedMessage(), ex);
        }
        return serialized;
    }
    
    
}
