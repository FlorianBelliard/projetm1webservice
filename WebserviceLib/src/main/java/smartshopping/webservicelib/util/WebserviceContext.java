/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartshopping.webservicelib.util;

import smartshopping.webservicelib.model.PersonDTO;

/**
 *
 * @author FLORIAN
 */
public class WebserviceContext {

    private String url;

    private PersonDTO person;

    public WebserviceContext(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public PersonDTO getPerson() {
        return person;
    }

    public void setPerson(PersonDTO person) {
        this.person = person;
    }
}
