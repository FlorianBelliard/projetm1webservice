package smartshopping.webservicelibclient;

import java.security.Security;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import smartshopping.webservicelib.accessservice.UserAccessService;
import smartshopping.webservicelib.exceptions.SerializationException;
import smartshopping.webservicelib.exceptions.ServiceException;
import smartshopping.webservicelib.exceptions.ServiceNotAvailable;
import smartshopping.webservicelib.model.UserDTO;
import smartshopping.webservicelib.util.WebserviceContext;

/**
 * Hello world!
 *
 */
public class App {

    public static void main(String[] args) {
        Security.addProvider(new BouncyCastleProvider());
        try {
//            ListAccessService listAccessService = new ListAccessService();
//            ListDTO list = listAccessService.getById(1);
//            list.setLabel("listTest1");
//            listAccessService.update(list);
            WebserviceContext webserviceContext = new WebserviceContext("https://31.32.185.233:8181/Webservice-1.0-SNAPSHOT/rest/");
//            WebserviceContext webserviceContext = new WebserviceContext("http://localhost:8080/Webservice/rest/");
            UserAccessService userAccessService = new UserAccessService(webserviceContext);
            UserDTO user = new UserDTO();
            user.setFirstName("bb");
            user.setLastName("bb");
            user.setLogin("bb");
            user.setPassword("bb");
            user.setAddress("bb");
            userAccessService.add(user);
//            UserDTO user = userAccessService.isUserExists("user1", "user1");
//            webserviceContext.setPerson(user);
////            ItemAccessService itemAccessService = new ItemAccessService(webserviceContext);
////            itemAccessService.searchByStore("reference1", 1);
//            ListAccessService listAccessService = new ListAccessService(webserviceContext);
//            ListDTO listDTO = listAccessService.getListOfUser(1);
//
//            ReferenceAccessService referenceAccessService = new ReferenceAccessService(webserviceContext);
//            ReferenceDTO referenceDTO = referenceAccessService.getById(3);
//
//            List<ListReferenceDTO> listReferences = listAccessService.getListReferencesOfList(1);
//
//            Map<ListReferenceDTO, EActionList> mapUpdate = new LinkedHashMap<ListReferenceDTO, EActionList>();
//            ListReferenceDTO newListReferenceDTO = new ListReferenceDTO();
//            newListReferenceDTO.setList(listDTO);
//            newListReferenceDTO.setReference(referenceDTO);
//            newListReferenceDTO.setQuantity(15);
//            mapUpdate.put(newListReferenceDTO, EActionList.ADD);
//            ListReferenceDTO updateListeReferenceDTO = listReferences.get(0);
//            updateListeReferenceDTO.setQuantity(25);
//            mapUpdate.put(updateListeReferenceDTO, EActionList.UPDATE);
//
//
//            listAccessService.updateListReferencesOfList(listDTO.getId(), mapUpdate);
//
//            System.out.println("OK");
//            StoreAccessService storeAccessService = new StoreAccessService(webserviceContext);
//            List<StoreDTO> stores = storeAccessService.getAll();
//            ListDTO list = new ListDTO();
//            list.setLabel("testList");
//            userAccessService.add(user);//            userAccessService.addListToUser(list, webserviceContext.getPerson().getId());
            //            List<ListDTO> lists = userAccessService.getListsOfUser(webserviceContext.getPerson().getId());
            //            StoreAccessService storeAccessService = new StoreAccessService(webserviceContext);
            //            StoreDTO store = new StoreDTO();
            //            store.setAddress("adresseTest");
            //            store.setLabel("storeTest");
            //            store.setPosition("positionTest");
            //            storeAccessService.add(store, 1);
            //            List<ListDTO> lists = userAccessService.getListsOfUser(1);
            //            ListDTO list = new ListDTO();
            //            list.setLabel("list2");
            //            list = userAccessService.addListToUser(list, 1);
//            System.out.println(stores.toString());
        } catch (SerializationException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServiceNotAvailable ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServiceException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
